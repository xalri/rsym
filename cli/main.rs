#[macro_use] extern crate rsym;
extern crate badlog;

use std::io::*;
use rsym::kernel::*;
use rsym::prelude::*;
use rsym::grammar;

fn main() {
   badlog::init(Some("info"));
   let mut e = Kernel::new();
   prelude(&mut e);

   let stdin = stdin();
   print!("> ");
   stdout().flush().unwrap();
   for line in stdin.lock().lines() {
      let line = line.unwrap();
      match grammar::expr(line.as_str()) {
         Ok(expr) => {
            let result = e.eval(expr);

            if !(result.is_sym() && result.as_sym().sym == sym!("Null")) {
               println!("{}", result);
            }
         },
         Err(e) => println!("Failed to parse: {}", e),
      };
      print!("> ");
      stdout().flush().unwrap();
   }
}