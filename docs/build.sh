#!/usr/bin/env bash
pandoc --highlight-style=espresso --filter=pandoc-citeproc \
       --biblio=references.bib --csl=chicago-author-date.csl \
       -o $1.pdf $1.md
