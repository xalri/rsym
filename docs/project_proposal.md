---
title: "Project Proposal : Computer Algebra Systems"
author: "Lewis Hallam"
bibliography: references.bib
geometry: margin=3cm
fontsize: 12pt
output: pdf_document
header-includes:
 - \usepackage{tikz}
 - \usepackage{gantt}
 - \linespread{1.1}
...


## Motivation and rationale

A computer algebra system (CAS) automates the manipulation of mathematical
expressions. Computer algebra systems are indispensable tools for
mathematicians, both for saving time and for performing manipulations which
would be nearly impossible by hand, and they do so without introducing errors.

At the core of a computer algebra system is term rewriting, the replacement of
expressions by application of a set of rewrite rules.  Term rewriting turns out
to be a powerful programming paradigm. An example of computation through
rewriting is Mathematica, a computer algebra system where complex expressions
are evaluated entirely through term rewriting.  Functions in Mathematica are
declared as rewrite rules which pattern match against their parameters, then
any instance of the function in future expressions are replaced by the
function's right hand side. Functions can have attributes such as `Flat` and
`Orderless` which represent properties like associativity and commutativity,
changing the behaviour of the pattern matching.

_The problem:_

The performance of the underlying rewrite system determines the size and
complexity of expressions the system can handle.  Commutative-associative
operators such as addition and multiplication pose a particular challenge for
performance, when terms get large, exhaustively searching through permutations
fails to produce results in any reasonable time, so alternative approaches are
needed.

This project aims to build the core of a computer algebra system with an
optimized rewrite system capable of applying rules for the simplification and
differentiation of large algebraic expressions.  The field of computer algebra
has been well studied, and many such systems already exist, the novelty
introduced by this project is the programming language in which it will be
implemented, a relatively new systems programming language named Rust which
focuses on speed, memory safety, and concurrency.

# Aim & Objectives

The aim of the project is to build a computer algebra system in Rust. The
objectives are to:

 - Write a parser for a language with specialized constructs for term
   rewriting.
 - Implement a conditional rewrite system for evaluating expressions in this
   language.
 - Define rules for simplification and differentiation of algebraic
   expressions.
 - Identify and understand methods of optimising term rewriting systems,
   particularly in matching associative-commutative operations.
 - Record the relative performance of various optimisations in the created
   system.

## Background

"Mathematica as a rewrite language" [@Buchberger96] describes Mathematica's
programming language (now known as the Wolfram Language) as a term rewriting
system: An expression is evaluated by repeated application of a set of rewrite
rules, the result is obtained when no more rules match, which we call the
_normal form_ of the expression. This paper was one of the motivations for the
direction this project has taken, it shows how powerful and expressive term
rewriting can be as a computational paradigm. The paper's overview of the
language's constructs will form the basis of the language implemented for this
project.


"Expreduce" [@Walker] is an open source CAS which has as it's core a conditional
rewrite system. It's language shares the syntax of Mathematica and other similar
systems, so the project will be a useful reference when writing a parser.


"Term rewriting and all that" [@Baader98]  is a textbook which introduces the
field of term rewriting. The ideas presented are useful for
optimising term rewriting systems. For example, in section 10.3, term
normalisation with associative-commutative (AC) operators is reduced to solving
linear diophantine equations, presumably leading to a much faster algorithm
than the naive approach.


"A compiler for Rewrite Programs in Associative-Commutative Theories"
[@Moreau98] describes an efficient method for term normalisation with AC
operators.  This is another potential optimisation which could be implemented
to speed up evaluation of large expressions.


"Flat matching" [@Kutsia] is about patten matching in flat theories. Flatness
is a property similar to associativity, but isn't limited to binary functions:
A flat theory is defined by
$f(\bar{x}, f(\bar{y}), \bar{z}) = f(\bar{x}, \bar{y}, \bar{z})$
where $\bar{x}, \bar{y}, \bar{z}$ are sequence variables consisting of 0 or
more terms.  In Mathematica, rather than addition and multiplication being
binary operators, `Plus` and `Times` can take any number of arguments
($Times[x] = x, Times[] = 1$) and are flat. Flatness is accounted for in
pattern matching; this paper describes both the theory and the practical
implementation of pattern matching with flatness.

## Project plan

The chart below shows a rough breakdown of tasks by week, starting at the week
beginning 2nd October 2017. The red block at the end of January represents exam
time.

\scalebox{0.8}{
\begin{gantt}[xunitlength=0.62cm,fontsize=\small,titlefontsize=\small]{13}{31}
   \begin{ganttitle}
      \titleelement{Oct}{4}
      \titleelement{Nov}{5}
      \titleelement{Dec}{4}
      \titleelement{Jan}{5}
      \titleelement{Feb}{4}
      \titleelement{Mar}{4}
      \titleelement{Apr}{4}
      \titleelement{May}{1}
   \end{ganttitle}
   \begin{ganttitle}
     \numtitle{1}{1}{31}{1}
   \end{ganttitle}

   \ganttbar{a}{0}{7}
   \ganttbar{b}{2}{4}
   \ganttbar{c}{7}{3}

   \ganttbarcon{d}{10}{4}
   \ganttbarcon{e}{14}{2}


   \ganttbar[color=red]{-}{16}{2}

   \ganttbar{f}{18}{2}
   \ganttbar{g}{18}{2}
   \ganttbar{h}{20}{2}
   \ganttbarcon{i}{22}{7}
   \ganttbar{j}{10}{21}

   \ganttcon{7}{2}{7}{4}
   \ganttcon{6}{3}{14}{6}
   \ganttcon{16}{6}{18}{8}
   \ganttcon{16}{6}{18}{9}
   \ganttcon{16}{6}{20}{10}
\end{gantt}
}

 a. Broad research into computer algebra systems and term rewriting.
 a. Build a prototype parser & rewrite system for simple algebraic expressions.
 a. Research the common syntax semantics used in rewrite languages.
 a. Implement the AST, parsing, and pretty printing. After this task the
    project's first objective will be complete.
 a. Naieve conditional term rewriting, taking into account commutativity &
    distributivity. This satisfies the project's second objective. The
    implementation will based on the prototype.
 a. Implement a simple command line interface, where expressions are reduced using
    a global rule table.
 a. Write rules for simplification and differentiation of expressions, completing
    the third objective.
 a. Write benchmarks for the rewrite system.
 a. Research and implement optimisations, recording the results of the benchmarks.
    This tasks will require the most effort, and is difficult to split into smaller
     tasks. At the end of this time the project's fourth and fifth objective will
     have been achieved.
 a. Write a dissertation, a compilation of everything learn throughout the project.
    Sections will be written as the relevant work is done in research and implementation
    then compiled at the end of the project for the final dissertation.

At the time of writing this proposal the project is on week 9.

In addition to the initial research into term rewriting, some brief research was
done into methods for writing parsers -- two common approaches are parser
generators, where a file describing a formal grammar is
compiled into code for parsing that grammar, and parser combinators, where
higher-order functions combine basic functions for parsing elements of the
grammar to form complete rules.

To assess the difficulty of the project a prototype was created. The prototype
includes a parser and rewrite system for  mathematical expressions.  Given an
expression and a set of (non-conditional) rewrite rules, the expression is
reduced to it's normal form. A basic set of rules for simplification was added
to simplify expressions.

This rewrite system is limited: There are no conditional rules; Rules are
applied in the order they're defined, rather than by some measure of
specificity; and pattern matching uses a naive algorithm for handling
commutativity.  Later versions will address these limitations.

# References
