---
title: "Term Rewriting for Computer Algebra"
author: "Lewis Hallam (150184050)"
date: "4th May 2018"
abstract:
    Term rewriting is the basis for the programming languages used in the popular computer
    algebra systems _Maple_ and _Mathematica_. This dissertation presents an interpreter 
    for a programming language based on Mathematica with non-linear associative-commutative
    pattern matching and sequence variables. A procedure is given for pattern matching 
    by exhaustive search, then we consider different classes of patterns 
    where more efficient matching algorithms can be used.
bibliography: references.bib
geometry: margin=3cm
fontsize: 11pt
output: pdf_document
header-includes:
 - \linespread{1.1}
 - \usepackage{enumitem}
 - \setlistdepth{9}
 - \setlist[itemize,1]{label=$\bullet$}
 - \setlist[itemize,2]{label=$-$}
 - \setlist[itemize,3]{label=$\textendash$}
 - \setlist[itemize,4]{label=$\cdot$}
 - \setlist[itemize,5]{label=$\cdot$}
 - \setlist[itemize,6]{label=$\cdot$}
 - \setlist[itemize,7]{label=$\cdot$}
 - \setlist[itemize,8]{label=$\cdot$}
 - \setlist[itemize,9]{label=$\cdot$}
 - \renewlist{itemize}{itemize}{9}
...

\small
Undergraduate dissertation for Computer Science.

Supervisor: Dr Victor Khomenko
\normalsize

_I declare that this dissertation represents my own work except where otherwise stated._

\newpage
\small
\tableofcontents
\normalsize
\newpage

# Introduction

A computer algebra system (CAS) automates the manipulation of mathematical
expressions. They have obvious advantages over working by hand in saving
time and performing tasks which would be infeasible by hand, and they do 
so without introducing errors.

At the core of a computer algebra system is term rewriting: the repeated
replacement of terms of an expression by application of a set of rewrite rules.
The performance of the underlying rewrite engine determines the complexity 
of expressions a CAS can handle -- commutative-associative operators pose a
challenge for performance.

The project's objectives are to:

 - Write a parser for a language with specialized constructs for term rewriting.
 - Implement an interpreter for evaluating expressions in this language.
 - Define rules for simplification and differentiation of algebraic expressions.
 - Identify and understand methods of optimizing term rewriting systems,
   particularly in pattern matching associative-commutative operators.
 - Record the relative performance of various optimisations.

In the following pages we will:

 - Review relevant background information on term rewriting and pattern matching,
   and their applicability to computer algebra;
 - Describe the syntax and semantics of a symbolic programming language for computer algebra;
 - Use the language to implement simplification and differentiation of algebraic expressions;
 - Detail a procedure for evaluating expressions in the language, focusing on pattern
   matching with sequences and commutative operators;
 - Consider programs which stress the performance of our interpreter and propose some
   solutions;
 - Finally, we reflect on the project, the process of implementing the language, and
   give some thoughts on further development.


\newpage

# Background

## Term rewriting

Transformations on algebraic expressions can be expressed as rewrite rules, pairs of terms written $l \rightarrow r$
which state that expressions in the form $l$ can be replaced by $r$.
A set of rewrite rules is called a term rewriting system (TRS). Terms are made of constants ($0$, $1$, ...),
operators ($+$, $*$, $sin$, ...), and variables ($a$, $b$, ...), and have a tree-like structure.

![Tree representation of the term $ax + b$](tree.png){width=100}

Consider symbolic differentiation with the operators $+$ and $*$, the following rules hold:

$$ D_x(x) \rightarrow 1 $$
$$ D_x(y) \rightarrow 0 $$
$$ D_x(\bold{a} + \bold{b}) \rightarrow D_x(\bold{a}) + D_x(\bold{b}) $$
$$ D_x(\bold{a} * \bold{b}) \rightarrow \bold{a} * D_x(\bold{b}) + \bold{b} * D_x(\bold{a}) $$

Here, $\bold{a}$ and $\bold{b}$ are variables. Variables in the left hand side of the rule (the pattern) can match any
term, then instances of the variable in the right hand side (the replacement) are replaced with the matched term.
Most of the time rewrite rules _reduce_ an expression to a smaller form, they're sometimes called reduction rules
or simply reductions. By repeatedly applying rules to some term we may find a form of the term where none of the rules
apply, this is called the term's _normal form_. A TRS in which every term has a normal form is _terminating_,
if no term has multiple possible normal forms the TRS is _confluent_.

An important part of applying term rewriting rules is pattern matching, the process of finding a map
from a pattern's variables to terms (a _substitution_) which transforms some pattern into a given term. If such a
subsitution exists the pattern matches the term. 
We denote substitutions $\sigma = \{\bold{a} \mapsto t_1, \bold{b} \mapsto t_2, ...\}$,
and the result of applying the substitution $\sigma$ to a term $t$ as $\sigma t$. In this notation, pattern matching 
is the process of finding $\sigma$ such that $\sigma p = t$ for a given pattern $p$ and term $t$.

_Term Rewriting and All That_ [@Baader98] gives an introduction to the field of term rewriting, including a linear time
algorithm for _syntactic matching_, pattern matching without consideration for associativity, commutativity, etc.
The book also covers unification, finding $\sigma$ such that $\sigma a = \sigma b$, which is less directly related to
this project, but will be mentioned a few times.

Extensions to the pattern matching algorithm allows for more complex rules to be defined, some extensions we'll consider
include:

 - Conditions: A conditional pattern is a pattern with an attached predicate which says whether or not a given substitution
   is valid.

 - Non-linearity: A term is linear if it contains at most one instance of each variable. We call a rewrite rule
   _left-linear_ if it's left-hand-side (the pattern) is linear. When matching a non-linear pattern, all instances of a
   variable must match the same term.

 - Commutativity: An operator $f$ is commutative if $f(a, b) = f(b, a)$. Note that commutativity
   can be written as a rewrite rule ($f(a, b) \rightarrow f(b, a)$) but this rule is non-terminating. 
   Commutativity is generalised to _orderlessness_, a property of n-ary functions where the result is the
   same regardless of the order of the arguments. When patterns contain orderless functions the pattern 
   matching algorithm treats the arguments as a multiset rather than a sequence.

 - Associativity: For an associative operator $f$ the pattern $f(a, b)$ can match the term $f(x, f(y, z))$ with either the
   substitution $\{a \mapsto x, b \mapsto f(y, z)\}$ or the substitution $\{a \mapsto f(x, y), b \mapsto z\}$. @KutsiaFlat
   discusses pattern matching with _Flat_ symbols, a generalisation of associativity to n-ary functions.
   An _AC_ function is one which is both associative and commutative.
   @ACBipartite gives a solution to pattern matching with AC functions using nested bipartite graphs.

 - Sequence variables: Sequence variables are variables which match a sequence of terms rather than a single term.
   @KutsiaSeq gives a procedure for unification with sequence variables. Combining all these extensions, @Krebber 
   considers non-linear conditional pattern matching with AC functions and sequence variables.


## Computer Algebra systems

_Rewriting as a computational paradigm_ [@RewritingComputation] talks about the use of term rewriting as a basis for
computation. Notably the programming languages of the computer algebra systems _Maude_ [@clavel00principles]
and _Mathematica_ [@Buchberger96] are based on term rewriting. The user defines rules to build up a rewrite system, then
expressions are evaluated by applying the rules to find the expression's normal form. A large collection of built-in
rules give optimised implementations of many common functions. Mathematica has particularly
powerful pattern matching, with multiple ways to express conditions, sequence variables which match multiple terms,
repeated patterns, and properties such as associativity and commutativity built into the languages idea of structural
equivalence.

To find the normal form of an expression in a language based on term rewriting we decide on an order to evaluate
sub-terms -- an order for visiting the nodes of the term when represented as a tree. We focus on left-most innermost
rewriting, where the sub-terms are evaluated left-to-right and bottom-to-top. _Term Rewriting with Traversal Functions_
[@Traversal] in it's introduction describes the purpose of tree traversal in term rewriting, then extends a term rewriting
language with user-defined traversal functions.

# A programming language based on term rewriting

This section gives an overview of the syntax and semantics of a programming language based on that used in Mathematica, 
where expressions are evaluated by application of a set of rewrite rules. The next section presents an interpreter for
the language.

First, a simple example.

    > fib[n:_] := fib[n - 1] + fib[n - 2]
    > fib[0] := 1
    > fib[1] := 1

    > fib[10]
    89

The language is most often used interactively. `>` represents a prompt for user input,
then the following line gives the result of evaluating the given expression.
In the example, a function `fib` is defined to calculate fibonacci numbers,
then called with an argument of 10, giving a result of 89.

The first three lines of the example define new rewrite rules.
Some rules are built-in to the language, like basic arithmetic; User-defined rules are added
with the set operator `=`, which evaluates the right hand side before creating the rule,
or the delayed set operator `:=`, which keeps the right hand side as written.

The left hand side of a rule can include _blanks_, denoted with an underscore `_`, which match any expression.

Patterns can be named to allow referencing the matched expressions in the right hand side of the rule.
`name:pattern` matches the pattern on the right and binds the matched expression to the name on the left.
The `:` can be omitted (`n_` rather than `n:_`) for brevity.
When multiple parts of a pattern are given the same name (a non-left-linear rule) the expressions they
match are required to be equal.

Named patterns are sometimes referred to as _variables_, though they differ from variables in term rewriting
in that they can encapsulate conditions, both in that names can be bound to patterns
with a complex structure (`a:(_+_)` matches a term only if it's the sum of two sub-terms), and in that
patterns can have attached predicates, as described in a later section.

Back to the example, after the rules are defined we evaluate the expression `fib[10]`.

Rules are checked in the reverse order they were defined, more recently defined rules are tested
first. For each rule the interpreter looks for a substitution which transforms
the left hand side of the rule into the expression. If a substitution is found, the expression is replaced by
the right hand side of the rule with the substitution applied.

Leftmost innermost rewriting is used: sub-terms are searched from left-to-right, bottom-up, and
evaluated fully before moving on.

In this example, `fib[n:_]` is the first rule to match, with the substitution `n = 10`, so `fib[10]`
is replaced with the right hand side of the rule and the substitution is applied, giving `fib[10 - 1] + fib[10 - 2]`.
`10 - 1` then matches the built in rule for subtraction, and is replaced with `9`,
`fib[9]` again matches the rule for `fib[n:_]`, and so on. The result is obtained when the expression
no longer matches any of the rules -- the expression is in normal form.


## Abstract Syntax

The abstract syntax of the language is minimal:

 - A term is either an atomic value or a function application
 - The atomic types are Symbols, Integers, Rationals, and Booleans
 - Function applications consist of an operator (represented by a symbol [^order]) and a list of terms (the arguments).

[^order]: This is a notable difference from Mathematica's language, where the operator can be
     any expression rather than only a symbol, allowing higher-order functions, currying, etc..

```
Term = Application | Symbol | Integer | Rational | Bool
Application = Symbol * seq of Term
```

Function applications are written with the operator followed by the arguments in square brackets delimited by commas:

`f[x, y, z]` is an application of the operator `f`, with arguments `x`, `y`, and `z`. In this context,
_operator_ and _function_ are used interchangeably.

Every expression can be written in this format, called _full form_, and _everything_ in the language is
an expression, including rule definitions. The pattern from the first example is shown again here, with it's full
form below:

    fib[n:_] := fib[n - 1] + fib[n - 2]

    SetDelayed[ fib[Pattern[n, Blank[]]], Plus[fib[Plus[n, -1], fib[Plus[n, -2]]] ]

Infix, prefix, and postfix operators are used as shorthand for common operations, and are converted
to full-form by the parser; The table below gives some of these operators in order of increasing
precedence next to their full form.

Shorthand         Full Form
--------------    -------------------------
`a = b       `      `Set[a, b]`
`a := b      `      `SetDelayed[a, b]`
`patt /; cond`      `Condition[patt, cond]`
`a + b + c   `      `Plus[a, b, c]`
`a - b       `      `Plus[a, Times[-1, b]]`[^normalization]
`a * b * c   `      `Times[a, b, c]`
`2a          `      `Times[2, a]`
`a / b       `      `Times[a, Power[b, -1]]`
`a ^ b       `      `Power[a, b]`
`name:patt   `      `Pattern[name, patt]`
`patt?test   `      `PatternTest[patt, test]`
`-a          `      `Times[a, -1]`
`_           `      `Blank[]`
`__          `      `BlankSequence[]`
`___         `      `BlankNullSequence[]`
`{a, b, c}   `      `List[a, b, c]`

[^normalization]: Note that subtraction, negation, and division are represented as equivalent expressions
    in terms of addition, multiplication, and exponentiation, the justification for this is given in a
    later section.

## Sequences

To express rules such as "Any product containing 0 is replaced with 0", we add the
concept of a blank sequence, denoted with two underscores `__`, which matches
one or more of the arguments of a function application.
As with `_`, we can bind the matched sequence of terms to a name:

    > f[a:__] := 2 * (a + 1)
    > f[1, 2, 3]
    14

$a$ is bound to a _sequence_ of expressions, which is simply an application of the `Sequence` operator with
the matched terms as it's arguments;
Names bound to sequences of expressions are sometimes called _sequence variables_.
In the above example, the expression matches the rule with the substitution $\{ a \mapsto Sequence \lbrack 1, 2, 3 \rbrack \}$,
and so is replaced with `2 * Plus[1, Sequence[1, 2, 3]]`.

When evaluating an expression, `Sequence`s are automatically _flattened_, their content is spliced in to
the parent:

    > f[a, Sequence[b, c], d]
    f[a, b, c, d]

So the expression from the example becomes `2 * Plus[1, 1, 2, 3]`, which evaluates to 14.

Three underscores `___` are used to represent a _blank null sequence_, which matches _zero_ or more terms.

    > f[a_, b___] = b
    > f[1]
    Sequence[] (* a matches 1, b matches the empty sequence *)

## Conditional patterns

Blanks can be made to only match particular types of expressions.

By adding a symbol after a blank, the blank will only match expressions if their _head_ matches the given symbol.
For atomic expressions, their head is a symbol representing their type, `Integer`, `Rational`, `Boolean`, or `Symbol`.
For function applications, their head is the operator, `Plus`, `Times`, `f`, `g`, and so on...

    > f[x:_Integer] = 0

    > f[3]   <- 3 is an integer, so the rule matches, and the expression replaced with 0
    0

    > f[a]
    f[a]     <- The rule's left hand side doesn't match, so the expression is unchanged

For more nuance, a _pattern test_ can be added with the `?` operator, making a pattern only match if
a named operator returns true when given the expression's value. Here we define a function `even` to
test if an expression is even, then use it as a pattern test for the function `f` so that the rule
only applies to even numbers.

    > even[x:_] := x % 2 == 0
    > f[x:_?even] := 42

    > f[3]
    f[3]    <- The expression is unchanged, since even[3] is false

    > f[4]
    42

When a pattern test is added to a blank sequence (`__`), all elements of the sequence must pass the test.

## Attributes

Symbols can be given attributes such as `Flat` and `Orderless`, which change how they're evaluated.

    > SetAttributes[f, Flat]
    > SetAttributes[f, Orderless]

    > f[2, f[1, 4], 3]
    f[1, 2, 3, 4]

The semantics of these attributes is described in more detail in the chapter on implementation.
If an operator has the `Orderless` attribute, it's arguments are automatically sorted into
canonical order. The attribute is also recognised in pattern matching, so patterns with an
`Orderless` operator can match with the arguments in any order.
The `Flat` attribute is used for associative operators. Nested calls to a `Flat` operator `f` are
flattened out.

Other attributes affect expression evaluation but not pattern matching. In particular the attributes
`HoldAll`, `HoldFirst`, and `HoldRest` delay the evaluation of the arguments of an expression. If
a function has the `HoldAll` attribute, we skip evaluating it's arguments and go directly to
looking for patterns which match the entire expression. `HoldFirst` skips the first argument, and
`HoldRest` skips all but the first element. An example is given in a later section.

## Lists

A list is denoted `{a, b, ...}`, which is shorthand for `List[a, b, ...]`.

The program below uses conditions, sequence variables, and attributes to define a function which
splits it's arguments into two lists, one containing odd values and the other containing even values.
A recursive function is defined which takes a value `n` and returns the sequence of natural numbers
$0, 1, ..., n$. Finally `split[seq[10]]` is evaluated, to split the numbers 0 to 10 by their parity.

    > split[l___?EvenQ, r___?OddQ] := {{l}, {r}}
    > SetAttributes[split, Orderless]
    > seq[n_] := Sequence[seq[n-1], n]
    > seq[0] := Sequence[]
    > split[seq[10]]
    {{2, 4, 6, 8, 10}, {1, 3, 5, 7, 9}}

## Procedural programming constructs

Procedural programming constructs such as branching and iteration can be implemented using pattern
matching and recursion:

    > SetAttributes[If, HoldRest]
    > If[true, then_, else_] := then
    > If[false, then_, else_] := else

`If` takes a condition and two expressions, if the condition is true, the first expression is evaluated,
otherwise the second is evaluated. `If` has the attribute `HoldRest` so only the predicate is evaluated
at the start, then after one of the two rules match, the resulting expression is no longer held, so is
evaluated.

This wouldn't be particularly useful if we could only evaluate a single expression, so we introduce
the `CompoundExpression` function, and a shorthand `a; b; c -> CompoundExpression[a, b, c]`.
A `CompoundExpression` evaluates it's arguments in order (so fist evaluates `a`, then `b`, then
`c`), then returns the value of the last argument.

    > x = 3; y = 2; x * y
    6

Loops can be implemented with recursion:

    (* Call the function f for each element of a list *)
    > ForEach[{head_, tail__}, f_Symbol] := f[head]; ForEach[{tail}, f]
    > ForEach[{head_}, f_Symbol] := f[head]

    > ForEach[{1, 2, 3}, Print]
    1
    2
    3


More examples of the language can be found in the tests at `rsym/tests/*.rsym` in the supplementary material.

# Symbolic simplification & differentiation

## Reducing expressions to a standard form

While procedural programs are interesting as an example of rewriting for general computation,
the purpose of the language is computer algebra. One of our goals is the automatic simplification
of expressions, which turns out to be quite difficult.

We may want expressions which are equivalent to all be transformed into the same simple form:
`a + a + a`, `2a + a`, `6/2 a`, etc, should all simplify to `3a`. This is called a canonical form [@Simplify].
@albrecht2013computer defines simplification as a function $S$ such that for any term $t$,
$S(t) \sim t$ and $S(t) \leq t$, where $\sim$ is equivalence (modulo commutativity & associativity in our case)
and $\leq$ is simplicity. @Carette formalises simplicity using information theory. For our purposes
we leave it undefined, and only change expressions when it's obvious that the result will be more simple.

A single canonical form is not always desirable. With polynomials for example, the factored form may be
shorter for some expressions while the expanded form is better for others
(in the definition above, this is the problem of defining $\leq$).
Our solution is to only define rules for the sorts of simplification where we can guarantee the right
hand side is more simple than the left.

The first step is to reduce the number of operators we have to consider. Negation, subtraction, and
division can all be represented in terms of addition, multiplication, and exponentiation. This
is handled by the parser, for example the expression `a - b` is parsed to `Add[a, Times[b, -1]]`.

We define a canonical order for orderless (commutative) expressions, so that `a + 1` and `1 + a`
both become `1 + a`. Orderless expressions are automatically sorted during evaluation. To handle
associativity, we let `Plus` and `Times` take any number of arguments and compute their sum and
product respectively. Both are given the attribute `Flat`, so `a + (b + c)` and `(a + b) + c` are
both automatically _flattened_ to become the same expression, `a + b + c`. These changes combine
many equivalent expressions into a single form, without changing their meaning.

Next, we add built-in rules for collecting the real part of sums & products. All integers and rationals
in a `Plus` or `Times` expression are removed and evaluated separately, then added back to the other terms.
If _all_ the terms are real the result is in the form `Times[n]`, which is then replaced by `n`.

Collecting like terms can be expressed with normal rewrite rules:

    x_ + x_ + o___ := 2 * x + o;
    (x_ * a_?NumberQ) + x_ + o___ := x * (a + 1) + o;
    (x_ ^ a_?NumberQ) * (x_ ^ b_?NumberQ) * o___ := x^(a+b) * o;

(NumberQ is a function which returns true if the parameter is an integer or rational, and false otherwise)

The rest of the process is done by rewrite rules which implement obvious simplifications, such as replacing
products containing `0` with `0`, removing `0`'s from sums, etc.

Some basic simplification can be done for trigonometric functions, like simplifying using identities,
replacing with an exact value, or wrapping the input based on the function's periodicity:

    Sin[x_] ^ 2 + Cos[x_] ^ 2 := 1;
    ...
    Sin[0] = 0;
    Sin[Pi/6] = 1 / 2;
    Sin[Pi/4] = Sqrt[2] / 2;
    ...
    Sin[Pi*(k_?NumberQ /; k > 2 || k < 0)] := Pi Mod[k, 2];
    Cos[Pi*(k_?NumberQ /; k > 2 || k < 0)] := Pi Mod[k, 2];
    Tan[Pi*(k_?NumberQ /; k > 2 || k < 0)] := Pi Mod[k, 2];
    ...

## Rules for differentiation

Relative to automatic simplification, differentiation is simple. 
We define a function `D[f:_, x:_]` to differentiate `f` in terms of `x`

     > D[x^2, x]
     2x

The following rules are used (@DifferentiationA, @DifferentiationB):

        
    (* Linearity *)
    D[a_ + b__, x_] := D[a, x] + D[Plus[b], x];
    D[a_ + b_, x_] := D[a, x] + D[b, x];

    (* Product rule *)
    D[a_*b_, x_] := b*D[a, x] + a*D[b, x];

    (* Power rule *)
    D[f_^g_, x_] := f^g * (D[f, x] / f * g + D[g, x]*Log[f]);
    D[x_^a_?NumberQ, x_] := a*x^(a - 1);
    D[Log[f_ /; f > 0], x_] := D[f, x] / f;

    (* Reciprocal rule *)
    D[f_ / g_, x_] := ((g * D[f,x]) - (f * D[g,x])) / g^2;

    (* Trigonometry *)
    D[Sin[x_], x_] := Cos[x];
    D[Cos[x_], x_] := -Sin[x];
    D[Tan[x_], x_] := 1 / Cos[x]^2;

    D[x_, x_] := 1;
    D[a_?NumberQ, x_] := 0;

The rules could be improved to account for non-numeric constants by adding a function which checks if an expression
contains some symbol, and use that as the condition in the last rule rather than `NumberQ` (such that for
any expression `a` not in terms of `x`, `D[a, x]` evaluates to 0).

# Evaluating expressions

The interpreter for the language described above has two major components: a command 
line interface, which parses user input and displays the result; and the kernel, 
which evaluates expressions. This section describes the algorithms used in the kernel 
with particular focus on the term rewriting engine. Parsing and formatting is touched on briefly.

The source code of the working interpreter is available at [https://gitlab.com/xalri/rsym](https://gitlab.com/xalri/rsym),
as well as in the supporting materials submitted with this dissertation.

## Data types & functions

The internal representation of expressions matches the abstract syntax with one small change:

    Term = Compound | Symbol | Integer | Rational | Bool
    Symbol = String * Integer
    Compound = Symbol * seq of Term

We define `Symbol` as containing a string, which gives the symbol as it's written, and
an integer used for scoping local variables in nested rules. For now only the string part is needed.

The program state consists of a stack of rewrite rules, an attribute map, and a list of _frames_, positions in the
rule stack used for scoping:

    State ::
        attributes : map Symbol to set of Symbol
        rules      : seq of (Term * (State -> Term * State))
        frames     : seq of Integer

Rewrite rules have two parts, the pattern (an expression), and the replacement function,
a function from program states to expressions and updated program states.

The kernel is made up of a few main functions:

 - $match$ takes a pattern and a sequence of terms and checks if they match. $match$ may change the state of the
   program: we represent substitutions as rewrite rules, so when a matching pattern is bound to a name, a replacement
   rule is added which maps the name to the matched term.

 - $replace$ takes a term and searches for and applies matching replacement rules to update the term.
   The replacement function may modify the program state, for example the built-in rule for evaluating $a := b$ adds
   the new rewrite rule to the rule stack. Replace only searches for rules which match the given term directly, not
   rules which match sub-terms of the term.

 - $recurse\_replace$ walks over the nodes of a term starting with the left-most inner-most sub-term and calls $replace$
   for each node.

 - $eval$ takes a term and evaluates it.


## Pattern Matching

The main bulk of the interpreter is the pattern matching & replacement engine, which takes an expression
and repeatedly applies rewrite rules to find it's normal form. This section gives the procedure for
pattern matching -- for a given pattern and an expression the procedure determines if they match,
accounting for sequence variables and associative functions and storing the substitution which transforms
the pattern into the expression.

We start with a procedure for _syntactic matching_, which walks over the expression tree and the pattern tree in step,
and returns false if any pair of sub-terms differ:

    fn match(pattern: Term, expr: Term) -> bool
        if expr is an atom
            return pattern == expr

        if op(pattern) == "Blank"
            return true

        if pattern and expr have the same operator and the same number of arguments
            // recurse for each argument, and return true only if all arguments match.
            for i in 0 to number of arguments
                if !match(pattern[i], expr[i])
                    return false
            return true

        return false

Sequence variables and orderless functions complicate the procedure slightly.
First we'll consider how to enumerate all the possible substitutions when the pattern
is orderless or contains sequence variables.

In general our algorithms don't take advantage of the structure of the
patterns: since non-linear patterns are allowed, all we can test is whether
or not a complete substitution is valid, we can't infer that if a single variable matches some
term in one substitution that it would also successfully match in another substitution. In general then,
when matching a function application, our goal is to generate the minimal
and complete set of substitutions and select one which is valid.

That said, exhaustive searching is slow, and most patterns are linear and contain only
simple conditions, so we later consider
ways to exploit additional knowledge about the pattern's terms to optimise the
interpreter for common pattern structures.


### Commutativity

Commutativity of a binary operation is generalised to n-ary functions with the property of
_orderlessness_. An orderless function gives the same result regardless of
the order of it's arguments: $f[\bar{a}, \bar{b}] = f[\bar{b}, \bar{a}]$ where $\bar{a}$ and
$\bar{b}$ are sequence variables.

This rule is non-terminating, since after replacing a term with the right hand side of the rule the
left hand side will still match. We instead extend the language's idea of structural equivalence
to understand orderlessness. We can consider the arguments of an orderless function to be a
multiset rather than a sequence.

Ignoring sequence variables for now, for orderless $f$ the pattern $f[a_1, a_2, ..., a_n]$ matches the term
$f[b_1, b_2, ..., b_n]$ iff there is a permutation $p$ of ${a_1, a_2, ..., a_n}$ such that for all
$i$, $p_i$ matches $b_i$.
The naive algorithm then is to iterate over permutations of the expression's arguments (using, for example,
Heap's algorithm [@Heap]), and attempt to match each one, giving $O(n!)$ performance.
If the terms are dependent on each-other (and so the only information we have is whether or not a _complete_
substitution is valid), and there are no duplicate terms in the target, this seems to be minimal.
If there _are_ duplicate terms in the target, permuting the sequence produces duplicate permutations, instead
we could use a multiset permutation algorithm [@MultisetPermutations].
Also, in our language the only dependency between sub-terms is non-linearity, which we can handle by attempting to unify
smaller substitutions. We'll consider faster algorithms in a later section.

### Blank Sequences

With blank sequences, a single term in a pattern can match multiple terms of a expression.

Consider the pattern $f[\bar{a}, \bar{b}, \bar{c}]$ where $\bar{a}$, $\bar{b}$, and $\bar{c}$ are sequence variables,
and a target $f[x, y]$. There are six possible substitutions which may transform the pattern into the target:

    a -> [x,y]  b -> []     c -> []
    a -> [x]    b -> [y]    c -> []
    a -> [x]    b -> []     c -> [y]
    a -> []     b -> [x,y]  c -> []
    a -> []     b -> [x]    c -> [y]
    a -> []     b -> []     c -> [x, y]

The problem of generating these substitutions can be visualised with a sequence of balls and
boxes, the boxes represent sequence variables and the balls the terms of the expression.
The boxes are arranged among the balls and each ball is added to the box to it's left.
We want to find all ways to arrange the boxes.

![Representation of sequence matching with balls and boxes](combinations.png)

Let $m$ be the number of boxes and $n$ be the number of balls.
The first box must be at position 0, since every term needs to be used. The other
$m-1$ boxes are distributed among the remaining $n + m - 1$ positions, so the positions
of the boxes are given by the ($m-1$)-combinations of $n+m-1$. [@BallsAndBoxes]

From the positions of the boxes, calculating the gap between two consecutive boxes
gives the number of balls in the left box.

This is implemented as a function $sequence\_combinations$ which takes $m$ and
$n$, and yields each combination, with the combination
represented as a list giving the number of balls in each box (=> the number of
terms of the expression to add to the sequence matched by each sequence variable
in the pattern).
For example, calling $sequence\_combinations$ with $n = 2$ and $m = 3$ generates the following
combinations, given in the same order as the substitutions in the example above:

    [2, 0, 0]
    [1, 1, 0]
    [1, 0, 1]
    [0, 2, 0]
    [0, 1, 1]
    [0, 0, 2]

The implementation is based on Algorithm T, lexicographic combinations, from Knuth's
The Art Of Computer Programming Volume 4 Fasc 3 [@TAOCP4_3]. The lexicographic combinations
algorithm is used to generate the ($m-1$)-combinations of $n+m-1$, then for each combination
$c$, the sequence combination $seq$ is  given by $seq_i = c_i - c_{i-1} - 1$, where $c_{-1} = -1$
and $c_n = n$. We update `seq` alongside `c` to avoid recalculating it for each combinations.

In pattern matching, for each combination we iterate over the pattern's terms and attempt
to match the $i$th pattern term against the next $seq[i]$ terms from the expression.

For patterns containing a mixture of $m_s$ sequence variables and $m_n$ normal variables
(and an expression with $n$ terms), $sequence\_combinations(n - m_n, m_s)$ gives the
number of terms to add to each sequence variable and each normal variable always takes a
single term.

### Combining commutativity & sequence variables

Orderless patterns containing sequence variables pose a challenge. There are two ways we might think
to combine the solutions above, neither are effective:

By first enumerating the sequence combinations then permuting the sequences, valid substitutions
are missed. Consider an orderless function taking
a sequence of integers and a sequence of symbols, `f[x__Integer, y__Symbol]`, when called
as $f[1, a, 2, b]$ no permutation of sequences of the expression will match.

By instead enumerating permutations of the terms, and then building all sequence combinations
for each permutation, we find the substitution $\{x \mapsto \lbrack 1,2 \rbrack, y \mapsto \lbrack a,b \rbrack \}$.
The problem here is that sequence variables in commutative patterns should match _multi-sets_ rather than sequences,
so building sequences for each permutation creates many duplicate substitutions: For the above
example the equivalent substitutions
$\{ x \mapsto \lbrack 1, a \rbrack, y \mapsto \lbrack 2, b \rbrack \}$ and
$\{ x \mapsto \lbrack a, 1 \rbrack, y \mapsto \lbrack b, 2 \rbrack \}$ are both generated,
even though, since the function is commutative, if the first didn't match then the second can't either.

A slightly better solution is to, for each sequence combination $s$, find all the ways to take
$s_0$ elements from the pattern for the first sequence, then take $s_1$ without replacement for the second, and so on.
We call this algorithm $multichoose$.

For a commutative pattern $f[\bar{a}, b, \bar{c}]$, a target $f[1, 2, 3, 4, 5, 6]$,
and the sequence combination $s = [2, 1, 3]$ (two elements in a, one in b as it's
not a sequence variable, and three in c), $multichoose$ gives the possible substitutions:

```
{a -> [1, 2], b -> 3, c -> [4, 5, 6]}
{a -> [1, 2], b -> 4, c -> [3, 5, 6]}
{a -> [1, 2], b -> 5, c -> [3, 4, 6]}
{a -> [1, 2], b -> 6, c -> [3, 4, 5]}
{a -> [1, 3], b -> 2, c -> [4, 5, 6]}
{a -> [1, 3], b -> 4, c -> [2, 5, 6]}
...
```

As soon as a valid substitution is found we can return.
This is the algorithm used in our interpreter, for the relative simplicity of implementation.
As above, Algorithm T [@TAOCP4_3] is used to generate k-combinations. We combine multiple
instances of the k-combinations iterator and track which to update next. The algorithm
can be found in `src/combinatorics.rs` in the supplementary material.

Note that we're still considering the arguments of the target as a sequence, rather than a multiset.
Say for the same pattern we had the target $f[1, 1, 1, 1, 1, 1]$, $multichoose$ would generate the same
number of possibilities, even though they're all equivalent.

A related problem in combinatorics, generation of multiselections, is presented by @Weider with a
non-recursive solution. An adaptation of this algorithm may solve the duplication problem.

In the domain of pattern matching, Manuel Krebber in their master's thesis _Non-linear Associative-Commutative
Many-to-One Pattern Matching with Sequence Variables_ [@Krebber] solves the problem by building a system of linear
equations which give the constraints of the problem where the variables are the number of each
element of the multiset of arguments in the target to include in each sequence variable in the
pattern. There are infinitely many solutions to the system, but restricting to positive numbers
gives a finite set of solutions. With non-linear patterns and arbitrary conditions, this solution is minimal.
This hasn't yet been implemented in our interpreter, but I expect that in addition to avoiding duplicated
test, it would also be faster than $multichoose$ in enumerating the substitutions.

### A matching procedure based on exhaustive search

This section gives the full pattern matching procedure for patterns with conditions and sequence variables, taking into
account associativity and commutativity. First, the semantics of a few language features we haven't yet discussed:

Named patterns (`name:pattern`, or in full form `Pattern[name, pattern]`) are simple, we call $match$ on the inner pattern, if it returns true,
a new rewrite rule is added which maps the name to the matched term.

On encountering a conditional pattern (`patterrn /; predicate` -> `Condition[pattern, predicate]`), match is called
with the inner pattern, the conditional pattern matches if both the inner pattern
matches and the predicate then evaluates to true. Since named patterns are added as rewrite rules
we don't need to apply any substitution to the predicate, when it's evaluated with
the current program state, any variables it contains are replaced by their matched terms by applying these rules.

The same applies to pattern tests (`pattern?predicate` -> `PatternTest[pattern, predicate]`), except the predicate is a function symbol, so
we evaluate `predicate[expression]` for every expression in the sequence of expressions
being matched.

As a final short note, normal variables in associative patterns can match multiple terms of an expression.
consider the pattern $a + b$ and the expression $x + y + z$. The variables in the pattern
act like sequence variables, the pattern can match with the substitution $\{ a \mapsto (x + y), b \mapsto z \}$.

So that's everything, here's all the parts combined into a working, but slow, algorithm:

$match$ takes a pattern $p$ and a sequence of $n$ terms $[t_1, t_2, .. t_n]$, and returns a bool. We use the
notation $t[i]$ to refer to the $i$'th argument of a term $t$, and $op(t)$ to refer to $t$'s operator.

 - If $p$ is atomic (not a function application):
    - Return true iff $n = 1$ and $t_1 = p$.
 - If $op(p) = BlankSequence$:
    - If $n = 0$ return false.
    - If $p$ has no arguments, return true.
    - Let $h = p[1]$ (the head we expect to match, e.g. $Integer$ for the blank `__Integer`)
    - Return true iff $\forall i \in {1..n}$, $head(t_i) = h$.
 - If $op(p) = BlankNullSequence$:
    - If $p$ has no arguments, return true.
    - Let $h = p[1]$.
    - Return true iff $\forall i \in {1..n}$, $head(t_i) = h$.
 - If $op(p) = Pattern$ (a named pattern):
    - Let $name = p[1]$, $pattern = p[2]$.
    - If $match(pattern) = false$, return false.
    - Check for an existing binding of $name$, if one exists, and the term it maps to is not equal to
      $[t_1, t_2, ..]$, return false.
    - Otherwise add the rewrite rule $name \rightarrow [t_1, t_2, ..., t_n]$ and return true.
 - If $op(p) = Condition$:
    - Let $pattern = p[1]$, $predicate = [2]$.
    - If $match(pattern) = false$, return false.
    - Return true iff $eval(predicate) = true$.
 - If $op(p) = PatternTest$:
    - Let $pattern = p[1]$, $predicate = [2]$.
    - If $match(pattern) = false$, return false.
    - Return true iff $\forall i \in {1..n}$, $eval(predicate[t_i]) = true$
 - If $n \neq 1$ return false. (at this point we've exhausted the types of patterns which may match a sequence)
 - If $op(p) = Blank$:
    - If $p$ has no arguments, return true.
    - Return true iff $head(t_1) = p[1]$
 - If $t_1$ is atomic, return false.
 - If $op(p) \neq op(t_1)$, return false.
 - Return $match\_args(p, t_1)$

So match handles atomic values, blanks, nested patterns, and named patterns, then delegates to a new function,
$match\_args$ for the less trivial task of testing matchings of the form
$f[a_1, a_2, ..] \stackrel{?}{\rightarrow} f[b_1, b_2, ..]$. We split the function into two since
$match$ is unlikely to change in optimisation.
$match\_args$ takes a pattern $p$ and a term $t$, both non-atomic, and attempts to match them by finding
a match between their arguments:

First we calculate the number of _free terms_ in $t$, the number of terms which are free to be assigned
to any sequence variable after the non-sequence variables have been filled. We can shortcut if there
are not enough terms to fill all the non-sequence variables:

 - Let $n_p$ be the number of arguments of $p$, and $n_t$ the number of arguments of $t$.
 - Let $v_n$ be the number of null sequence variables in $p$.
 - If $n_t < n_p - v_n$ return false.
 - Let $v_s$ be the number of non-null sequence variables in $p$.
 - If $v_n + v_s = 0$ and $n_t > n_p$ return false.
 - Let $v_{free} = n_t + v_n - n_p$.

Then we branch depending on whether the operator is orderless. If it's not, we iterate through the different
ways to arrange the free arguments among the pattern's sequence variables, and attempt to match each one:

 - Let $attrs$ be the attributes associated with $op(p)$.
 - If $Orderless \notin attrs$:
    - 'seq_loop': for $c$ in $sequence\_combinations(v_{free}, v_n + v_s)$:
        - Let $j = 0, k = 0$
        - For $i$ in $\{0, 1, ..., n_p\}$
            - If $p[i]$ is a sequence variable (null or otherwise):
                - Let $start = j$
                - Set $j = j + c[k]$
                - If $p[i]$ is a _non-null_ sequence variable, increment $j$ by one.
                - Increment $k$ by one
                - Let $seq = t[start..j]$
            - Else:
                - Increment $j$ by one.
                - Let $seq = t[j-1 .. j]$
            - If $match(pattern, seq)$ returns false:
                - Remove any rewrite rules we added.
                - Skip to the next iteration of 'seq_loop'
        - If we reach the end of this loop, all sequences matched, return true.
    - At this point none of the sequence combinations matched, return false.

For orderless operators, we enumerate the sequence combinations, for each sequence we modify it to contain
non-sequence variables, then pass it to $multichoose$ along with $t$ to enumerate the ways of arranging the
terms of $t$ among the variables in $p$:

 - If $Orderless \in attrs$:
    - For $c$ in $sequence\_combinations(v_{free}, v_n + v_s)$:
        - Let $seq$ be an array of $1$s length $n_p$.
        - Let $k = 0$
        - For $i$ in $\{0, 1, ..., n_p\}$:
            - If $p[i]$ is a sequence variable (null or otherwise):
                - Increment $k$.
                - Set $seq[i] = c[k-1]$.
                - If $p[i]$ is a _non-null_ sequence variable, increment $seq[i]$ by one.
        - 'part_loop': for $partition$ in $multchoose(seq, t)$
            - For $i$ in $\{0, 1, ..., n_p\}$:
                - If $match(p[i], partition[i])$ is false
                    - Remove any rewrite rules we added.
                    - Skip to the next iteration of 'part_loop'.
            - If we reach the end of this loop, all partitions matched, return true.
    - No partition of any combination matched, return false.

Next we'll look at some ways to improve performance for certain classes of patterns, before moving on to the semantics
of $eval$.


### A linear algorithm for simple commutative sequence patterns

Consider the following commutative function which splits it's arguments into a list of even numbers and a list
of odd numbers:

    > SetAttributes[split, Orderless]
    > split[even:(___?EvenQ), odd:(___?OddQ)] := {{even}, {odd}}

    (* seq[n] generates the sequence [1, 2, ..., n]: *)
    > seq[n_] := Sequence[seq[n-1], n]
    > seq[0] := Sequence[]

    > split[seq[10]]
    {{2, 4, 6, 8, 10}, {1, 3, 5, 7, 9}}

Clearly the result of `split[seq[n]]` can be calculated in $O(n)$ time, by testing the parity of each number ${1, 2, ... n}$
then assigning it to the correct set. The table below shows the performance of the implementation as described so far:

Term                Evaluation time
-------------       ----------------------------
split[seq[5]]       27,335 ns/iter (+/- 961)
split[seq[6]]       57,929 ns/iter (+/- 1,046)
split[seq[7]]       67,127 ns/iter (+/- 1,721)
split[seq[8]]       172,402 ns/iter (+/- 9,920)
split[seq[9]]       213,329 ns/iter (+/- 12,693)
split[seq[10]]      610,267 ns/iter (+/- 18,469)
split[seq[11]]      787,469 ns/iter (+/- 18,254)
split[seq[12]]      2,344,838 ns/iter (+/- 70,029)

So the current system takes on the order of $2^n$ time, which isn't very good.

To improve the performance, we note two things about the definition of `split`:

 - It's left-linear (each variable occurs exactly once in the pattern).
 - It's sequence variables are all _simple_: the conditions are pattern tests on _each element_ of the
   sequence, not the sequence as a whole. If one of the arguments can belong to the sequence `even`, it can
   do so regardless of the other elements of `even`.
   (contrast to `(s___) /; Plus[s] == 10` where `10` can only belong to `s` if the rest of
   the elements sum to `0`):

If we can define simplicity, we can write an algorithm which takes the class of patterns
which are simple and linear, and test for a match in much less time.
A possible definition is that a pattern is non-simple only if it contains a sequence variable
with some condition on the sequence as a whole. A sequence variable with a pattern test is simple,
non-sequence variables (even those with conditions) are simple, and so on.

The new algorithm is based on the bipartite matching of the _non sequence_ variables
of the pattern to the arguments of the target.
We construct a bipartite graph, matching non-sequence pattern terms to target arguments, then look
for a perfect bipartite matching, if none exist we return false.
For each perfect matching (@EnumMatchings gives an algorithm for quickly enumerating perfect
matchings) we test the remaining terms (those not matched to a non-sequence variable) against the
conditions for the sequence patterns, and assign them to the first sequence pattern they match.

Non-null sequence variables are used both in the perfect matching step (to guarantee they
contain at least one element) and when distributing the remaining terms.

A nice property of this is that the bipartite matching step is skipped if we only have
null sequence variables.

For example, for the `split` pattern above, we can see that all the terms are simple
and that no variable occurs more than once: First the bipartite matching, there are no
non-sequence terms of the pattern, so nothing is matched, and we skip to the next step:
for each term of the pattern, we test it against the condition for the first sequence
variable, then the condition for the second, and assign the term to the first one where
the condition is satisfied. If for any term of the target, it doesn't match any
term of the pattern, there is no match. `1` fails the `EvenQ` condition, but passes the
condition `OddQ`, so is added to `odd`, `2` passes `EvenQ` so is added to `even`, and so on,
until there are no more terms of the target, and we have values for `even` and `odd`. In
theory, this process finishes in linear time.

I'm not convinced that with the informal definition for simplicity given above this
algorithm will always give the same results as exhaustively testing combinations, it
might be that there are counter-examples which require further restricting the class
of simple patterns, but it seems correct for at least patterns consisting of
only sequence variables with pattern tests, so we implemented that.
`split[seq[n]]` is now evaluated in linear time, as shown in the table below. 

Term                Evaluation time
-------------       ----------------------------
split[seq[5]]       13,225 ns/iter (+/- 655)
split[seq[6]]       15,808 ns/iter (+/- 440)
split[seq[7]]       18,400 ns/iter (+/- 242)
split[seq[8]]       20,970 ns/iter (+/- 659)
split[seq[9]]       23,581 ns/iter (+/- 1,154)
split[seq[10]]      26,135 ns/iter (+/- 319)
split[seq[11]]      28,741 ns/iter (+/- 1,223)
split[seq[12]]      31,297 ns/iter (+/- 477)

The bipartite matching solution for eliminating non-sequence variables has not yet been implemented.

### Matching constants early

For commutative patterns containing constants, the number of terms we need to consider when enumerating permutations
can be reduced by matching the constants first.
For each constant in the pattern look for a matching constant in the target. If one is found, remove it from both the 
pattern and the target. Continue until all constants in the pattern have been tested. This can be implemented without
any performance impact for patterns without constants: the canonical ordering of the arguments of a commutative function
means the constants are always first, so we can stop looking when we see a non-constant term.

## Rewrite rules

So far we've described a function `match` which takes a pattern and a sequence of expressions and returns
true if the pattern matches the sequence. The next step is to define a procedure for applying a set of
rewrite rules.

The built-in rules are added to the rule list when the kernel is created. To allow user defined rules, 
we add built-in rules for `Set` and `SetDelataed` which add the rule defined by their arguments to the 
kernel's set of rewrite rules, and in the case of `Set`, returns the evaluated replacement.

The implementation of the set functions makes some changes to the pattern and replacement before creating the rule:
Consider the identity function `id[x:_] = x`, which takes an expression and returns it without any changes.
When invoked as `id[x]`, the expected result is the symbol `x`, but instead it will recurse until
it overflows the stack: `id[x]` matches the pattern `id[x:_]` with the
substitution $\{ x \mapsto x \}$, which, when represented as a rewrite rule, is non-terminating.
The solution is to make the two `x`s differ somehow. So before adding the rule, the replacement function for `Set` walks 
over the pattern searching for variables, and stores a rule which replaces the name with the same symbol but with a 
unique value of `n` (the integer field we added to `Symbol` for this purpose).
After finding all the variable symbols we apply the replacement rules (to make the symbols unique) to both the pattern
and the replacement, then add the rule.
Now, when we match `id[x:_] = x` with the substitution $\{ x \mapsto x \}$, the first and second `x` are different,
since the first has some unique value of `n`, and so the rule terminates.

Earlier we noted that substitutions are handled by adding rewrite rules while pattern matching. These intermediary
rules need to be deleted after the right hand side has been evaluated so they don't apply to any future patterns. 
We don't want for example `f[x_] := 3; f[2]; x` to evaluate to `2`, since the `x` at the end is outside the function's scope.
Our solution is analogous to the stack in a procedural programming language, when we apply a replacement rule
we enter a new stack frame by pushing the current size of the rule stack to a list of frames, then when we leave that 
frame all of the replacement rules defined within it are removed.
For `Set` and `SetDelayed`, we want the added rule to persist even if the expression is nested in some other expression,
to do so we insert it into the top of the first stack frame, then increment the other frame indices to point to the right place.

Applying replacement rules then is simple. we iterate through all the rules (both builtin and those defined by `Set`)
starting from the most recently defined rule, and call `match` to test if each pattern matches. Before calling match
we create a new stack frame, then after, if the pattern doesn't match, the frame is removed along with any new rules. 
Note that we can't store the size of the rule stack locally, it needs
to be part of the program state for it to be updated if any global rules are added by the replacement function.
When we find a rule which matches we run the replacement function, which returns the new expression (and may also, if it's a builtin function, change the
program state).

## Semantics of $eval$

The full evaluation procedure has a few extra tasks to perform in addition to finding and applying rewrite rules.
$eval$ is a function which takes an expression $e$, and returns an expression:

 1. If $e$ is atomic, skip to step 6.
 2. Let $attrs$ be the attributes associated with $op(e)$.
 3. First, evaluate the arguments:
    - If $HoldAll \in attrs$ skip to step 4.
    - If $HoldFirst \notin attrs$, set $e[0]$ to $eval(e[0])$
    - If $HoldRest \notin attrs$, for all $i \in {1, 2, ..}$ set $e[i]$ to $eval(e[i])$
 4. Flatten the expression:
    - If $Flat \in attrs$, for each of $e$'s arguments with the operator $op(e)$, splice in that expression's arguments.
    - For each argument with operator $Sequence$, splice in that expression's arguments.
 5. If $Orderless \in attrs$, sort the arguments into canonical order.
 6. Set $e = replace(e)$. If $e$ has changed, go back to step 1, otherwise return $e$.

## Notes on Performance

This section covers some non-algorithmic performance considerations.

#### Associating rules with symbols

Our general goal is to reduce the number of calls to `match` needed to evaluate an expression by exploiting knowledge
about classes of patterns. One obvious improvement is to store patterns with the same operator together, then
when iterating over rules, only look at the rules associated with the expression's head.

A hash map from symbols to rewrite rules would be the first choice, but scoping complicates the implementation a little.
We instead store a hash map of symbols to indices into the rule stack, then iterate over only the subset of the
associated rules which are in the desired stack frame. When new global rules are added the associations have to be
updated to point to the correct index.

### String interning

The simplest way to represent symbols in the interpreter is as strings. This is relatively
inefficient: We usually have many instances of the same symbol, so memory is wasted, and
string comparisons are expensive.

With string interning, we keep only a single copy of each symbol in a pool of symbols,
and reference them by index. Creating (parsing) symbols becomes more expensive, since we
need to test if the string is already in the global pool before creating it, but memory
usage is reduced, and comparisons avoid fetching the string by comparing the indices.

Symbols created in the source code as literals can be compiled directly to their index, so
don't have to be created at run-time. The interning library we used,
string-cache [@StringCache], allows a perfect hash function [@BelazzouguiHash] to be generated
at compile time for a given set of strings, speeding up creation of the built-in symbols ("Blank", "Set", "Plus", etc).

### Storing expressions contiguously

During pattern matching we access sub-terms in a linear order, so it may help to store
expressions contiguously to reduce cache misses.
This is one of the principles of _data oriented design_ in games engineering [@LlopisDOD], [@DiceDOD], which
calls for designing programs such that data which is used together is stored together. The same memory may also
be reused to avoid allocations when creating temporary expressions (e.g. for substitutions in pattern matching).

### Memoization

The expansion of `fib[n]` contains many identical recursive calls:

                                            fib[n] =
                                     fib[n-1] + fib[n-2] =
                          fib[n-2] + fib[n-3] + fib[n-3] + fib[n-4] =
    fib[n-3] + fib[n-4] + fib[n-4] + fib[n-5] + fib[n-4] + fib[n-5] + fib[n-5] + fib[n-6] =
                                             ...

For recursive functions with multiple recursive calls we can improve performance with
memoization -- remembering the results of previous evaluations of an expression to avoid
repeated calculations.

Memoization can be added for a particular function without any changes to the interpreter
by combining the `Set` (`=`) and `SetDelayed` (`:=`) operators:

    > fib[n_Integer] := fib[n] = fib[n-1] + fib[n-2]
    > fib[0] = fib[1] = 1

    > fib[50]
    20365011074




# A command line interface

The user interacts with the kernel through a simple command line interface. The interpreter reads an
expression typed into the terminal, evaluates the expression, and prints the result (sometimes called
a REPL, a Read-Eval-Print Loop).

The first step in evaluating some input is to parse it into an expression tree:

The concrete syntax of the language is specified in a parsing expression grammar [@PEG], from which a parser
generator, rust-peg [@rust-peg], creates a recursive decent parser. The definition format of rust-peg
includes syntax for defining infix operators -- the generated code uses precedence
climbing [@Clarke86] to parse left and right associative infix operators into a tree.

The conversion from tokens to the expression tree is defined inline in the grammar, so the generated
parser is a function which takes a string and returns either an expression tree or, if the given
string is invalid, an error giving the location of the error and a list of expected symbols.

The full grammar can be found at `engine/src/grammar.peg` in the interpreter's source code.
A few notes:

  - Negation, subtraction, and division are converted to equivalent expressions in terms of
    addition, multiplication and exponentiation by the parser (e.g. `a - b` is parsed as
    `Plus[a, Times[b, -1]]`).
  - There's no built-in functionality for pre/post-fix operators with precedence, so all
    pre-fix and post-fix operators have higher precedence than infix operators.
  - To parse "2x" as a multiplication, we would like to use an infix operator at the same precedence
    level as multiplication with an empty string as the infix symbol, but since unary negation
    `-` has a higher precedence, `a - b` is parsed as `a (-b)`, a multiplication. Instead, multiplication
    must be explicit ($2*x$ rather than $2x$).

The inverse of parsing, converting from an expression back into a string, is used to display the result
of an expression to the user.
We traverse the expression tree, replacing expressions with known heads like `Plus` with their operator
form and adding brackets, falling back to full form for expressions without an operator form. An improvement
to this would be to track precedence of operators as we descend the tree and remove unnecessary brackets.


# Evaluation & Conclusions

## Evaluation of the project's objectives

> _Write a parser for a language with specialized constructs for term rewriting._

This objective was met early in the project, and was easier than I expected. Once I found the right tools
(a PEG parser generator with built-in infix support) writing the grammar itself was simple. Being based on
Mathematica, there wasn't much of a design component to this goal, the main design decision was which sub-set of 
features should be included to give a powerful language without over-complicating the implementation.

> _Implement an interpreter for evaluating expressions in this language._

This objective has been met.

> _Define rules for simplification and differentiation of algebraic expressions._

It's hard to measure this one since we can't easily define simplification. For basic expressions the objective is met.
It could be improved with simplification of radicals, rules for logarithms, and expansion & factorisation
of polynomials with some measure of which form is more simple for a given polynomial. Rules for differentiation are 
well known, an improvement here would be more extensive testing.

> _Identify and understand methods of optimizing term rewriting systems,
  particularly in pattern matching associative-commutative operators._

This was made difficult by the unusual pattern matching of Mathematica. I only found one source for
pattern matching with sequence variables [@Krebber]. The well known technique of bipartite matching
for commutative patterns was modified to possibly work with sequence variables, but the algorithm
wasn't implemented. I'd consider the objective mostly unmet (in particular I'd have liked to explore
JIT compilation of patterns into machine code if I had more time).

> _Record the relative performance of various optimisations._

This objective wasn't met. While developing the interpreter so many of it's parts were changing that it was difficult
to get a measure of the improvement of a particular change. The nature of the benchmarks also changed as I got a 
better understanding of how to stress different parts of interpreter (calculating fibonacci numbers to test raw
recursion speed, or commutative patterns with conditional sequences to test the speed of commutative matching.)
That aside, there were not many optimizations which seemed suitable to include in this dissertation, things like
switching to a different library for arbitrary precision numbers gave a great performance improvement, but isn't
relevant outside of this particular implementation. As it is, the short sections on non-algorithmic optimizations seem
out of place.

## What has been learnt

I enjoyed the research components of the project. There's not much introductory material on term rewriting, it was
interesting to piece together a basic understanding of the field by flipping between reading chapters of books and 
various articles, going back to articles I'd read previously with a better understanding of the concepts and terminology. 
I've learnt that it's worth it to spend a good amount of time reading and searching for articles before starting to write, some 
sources I found very close to the end of the project would have been much more useful had I found them earlier (@Krebber
in particular has a fantastic preliminaries section and points to many good references).

The actual implementation of the interpreter was the part I was most comfortable with, it was good practice in
efficiently implementing algorithms.

## Notes on the software engineering process

The interpreter is a relatively small project, just over 1000 LoC, for what that metric's worth, 
not large enough to justify following any particular software engineering methodology or paradigm. 
Testing was important, particularly when optimizing, to give some confidence in the correctness of 
algorithms as they change. The interpreter was written in Rust ([https://rust-lang.org](https://rust-lang.org)),
which has a testing and benchmarking framework as part of it's build tool: unit tests are written inline
in the program's source code, and run on compilation in the IDE. Benchmarks are run manually, and give the average
time and spread in nanoseconds for multiple iterations of each micro-benchmark -- useful for testing that optimizations
actually improve the performance of the interpreter.

## Further work

While being a useful learning experience, the interpreter has a long way to go to be useful in practice. Some of the
possible improvements include:

- Further algorithmic improvements to pattern matching;
- JIT compilation of patterns into machine code;
- Better handling of numbers (currently only arbitrary precision integers and rationals are implemented, 
  Notably missing are machine precision integers and rationals, floats, reals, complex numbers and infinite values);
- The command line interface could be improved. Features such as referring to previous answers, pressing up to modify
  old expressions, and minimising unnecessary brackets in the output would help improve the experience;
- A large difference from Mathematica's language is that it's first-order -- Mathematica allows the 'operator' of an expression
  to be another expression, rather than just a symbol. Higher order functions are useful when writing
  larger programs.


# References
