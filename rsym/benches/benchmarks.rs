#![feature(test)]
#![feature(decl_macro)]

extern crate test;
extern crate rsym;
use test::*;

use rsym::{kernel::*, prelude::*, grammar};

macro bench_fib($i:ident, $n:expr) {
   #[bench]
   fn $i(b: &mut Bencher) {
      let mut k = Kernel::new();
      prelude(&mut k);
      k.eval(grammar::expr("fib[n_] := fib[n - 1] + fib[n - 2]; fib[0] = fib[1] = 1").unwrap());
      let test = grammar::expr(&format!("fib[{}]", $n)).unwrap();
      b.iter(|| {
         k.eval(test.clone());
      });
   }
}
bench_fib!(bench_fib_09, 9);
bench_fib!(bench_fib_10, 10);
bench_fib!(bench_fib_11, 11);
bench_fib!(bench_fib_12, 12);

macro bench_split($i:ident, $n:expr) {
   #[bench]
   fn $i(b: &mut Bencher) {
      let mut k = Kernel::new();
      prelude(&mut k);
      k.eval(grammar::expr(r#" split[x__?EvenQ, y__?OddQ] := {Sort[{x}], Sort[{y}]}; SetAttributes[split, Orderless]; seq[n_] := Sequence[seq[n-1], n]; seq[0] := Sequence[] "#).unwrap());
      let test = grammar::expr(&format!("split[seq[{}]]", $n)).unwrap();
      b.iter(|| k.eval(test.clone()));
   }
}
bench_split!(bench_split_05, 5);
bench_split!(bench_split_06, 6);
bench_split!(bench_split_07, 7);
bench_split!(bench_split_08, 8);
bench_split!(bench_split_09, 9);
bench_split!(bench_split_10, 10);
bench_split!(bench_split_11, 11);
bench_split!(bench_split_12, 12);

#[bench]
fn bench_parser(b: &mut Bencher) {
   let src = include_str!("parser_bench.rsym");
   b.iter(|| {
      grammar::expr(src).unwrap()
   });
}
