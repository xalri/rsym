#![feature(test)]

extern crate test;
extern crate rsym;
use test::*;

use rsym::combinatorics;

#[bench]
fn seq_combinations(b: &mut Bencher) {
   b.iter(||{
      combinatorics::sequence_combinations(black_box(10), black_box(4)).count();
   })
}

#[bench]
fn multichoose(b: &mut Bencher) {
   b.iter(||{
      combinatorics::multichoose(&[4, 3, 3], &[&1, &2, &3, &4, &5, &6, &7, &8, &9, &10]).count();
   })
}

#[bench]
fn k_combinations(b: &mut Bencher) {
   b.iter(||{
      combinatorics::k_combinations(black_box(10), black_box(4)).count();
   })
}
