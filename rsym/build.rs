extern crate peg;
extern crate string_cache_codegen;

use std::env;
use std::path::Path;

fn main() {
   peg::cargo_build("grammar.peg");

   string_cache_codegen::AtomType::new("sym::Sym", "sym!")
      .atoms(vec![
         // Common variable names
         "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m",
         "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z",

         // Operators
         "Plus",
         "Times",
         "Power",
         "Not",
         "And",
         "Or",
         "Unequal",
         "Equal",
         "Greater",
         "Less",
         "GreaterEqual",
         "LessEqual",

         // Basic types
         "Symbol",
         "Rational",
         "Integer",
         "Boolean",
         "String",
         "Null",
         "Sequence",

         // Attributes
         "HoldAll",
         "HoldFirst",
         "HoldRest",
         "SequenceHold",
         "Flat",
         "Orderless",

         // General
         "Set",
         "SetDelayed",
         "CompoundExpression",
         "List",
         "SetAttributes",
         "ClearAttributes",

         // Pattern matching
         "Blank",
         "BlankSequence",
         "BlankNullSequence",
         "Pattern",
         "Condition",
         "PatternTest",
      ])
      .write_to_file(&Path::new(&env::var("OUT_DIR").unwrap()).join("sym.rs"))
      .unwrap()

}
