//! Iterating over permutations & combinations

use mem::*;

// ============================================================
// --------------------- k-combinations -----------------------
// ============================================================

/// Iterate through the k combinations of {0, 1, ..., n - 1}
pub fn k_combinations(n: usize, m: usize) -> KCombinations {
   assert!(m <= n);
   let mut iter = KCombinations {
      n, m, j: m,
      c: block!(usize, m+2),
      done: false,
      start: true,
   };

   iter.reset();
   iter
}

/// An iterator over k-combinations
pub struct KCombinations {
   n: usize,
   m: usize,
   j: usize,
   c: Block<usize>,
   done: bool,
   start: bool,
}

impl Iterator for KCombinations {
   type Item = &'static [usize];
   fn next(&mut self) -> Option<&'static [usize]> {
      let KCombinations{ m, mut j, ref mut c, .. } = *self;

      if self.done { return None; }
      if self.start {
         self.start = false;
         if self.m == self.n { self.done = true; }
         return Some(&c.get()[0..m])
      }

      let mut x;
      if j > 0 {
         x = j;
      } else {
         if c[0] + 1 < c[1] {
            c[0] += 1;
            return Some(&c.get()[0..m]);
         }

         j = 2;

         loop {
            c[j - 2] = j - 2;
            x = c[j - 1] + 1;
            if c[j] != x { break }
            j += 1;
         }

         if j > m {
            self.done = true;
            return None;
         }
      }

      c[j-1] = x;
      self.j = j - 1;
      Some(&c.get()[0..m])
   }
}

impl KCombinations {
   /// Reset the iterator to the first combination
   pub fn reset(&mut self) {
      let n = self.n;
      let m = self.m;
      if n == 0 || m == 0 {
         self.done = true;
         return;
      }

      self.j = m;
      self.done = false;
      self.start = true;
      for i in 0..m { self.c[i] = i }
      self.c[m] = n;
      self.c[m+1] = 0;
   }
}


// ============================================================
// ----------------- sequence combinations --------------------
// ============================================================

/// Iterate through the sequence combinations of {0, 1, ..., n - 1}
pub fn sequence_combinations(n: usize, m: usize) -> SeqCombinations{
   let mut iter = SeqCombinations {
      n, m, j: 0,
      seq: block!(usize, m),
      c: block!(usize, if m > 0 { m+1 } else { 0 }),
      done: false,
      start: true,
   };

   iter.reset();
   iter
}

/// An iterator over sequence combinations. The same memory is reused for each
/// combination, so the yielded slice should't be stored.
// TODO: caching combinations for small values m and n?
pub struct SeqCombinations {
   n: usize,
   m: usize,
   j: usize,
   seq: Block<usize>,
   c: Block<usize>,
   done: bool,
   start: bool,
}

impl Iterator for SeqCombinations {
   type Item = &'static [usize];
   fn next(&mut self) -> Option<&'static [usize]> {
      let SeqCombinations{ m, n, mut j, ref mut c, ref mut seq, .. } = *self;

      if self.done { return None; }
      if self.start {
         self.start = false;
         if n == 0 || m == 1 { self.done = true }
         return Some(seq.get())
      }

      let mut x;
      if j > 0 {
         x = j;
      } else {
         if c[0] + 1 < c[1] {
            c[0] += 1;
            seq[0] += 1;
            seq[1] -= 1;
            return Some(seq.get())
         }

         j = 2;
         loop {
            c[j - 2] = j - 2;
            seq[j - 1] = seq[j - 2];
            seq[j - 2] = 0;
            x = c[j - 1] + 1;
            if c[j] != x { break }
            j += 1;
         }

         if j > m - 1 {
            self.done = true;
            return None;
         }
      }

      c[j-1] = x;
      seq[j-1] += 1;
      seq[j] -= 1;

      self.j = j - 1;
      Some(seq.get())
   }
}

impl SeqCombinations {
   /// Reset the iterator to the first combination
   pub fn reset(&mut self) {
      let n = self.n;
      let m = self.m;
      if m == 0 { return; }
      self.j = m - 1;

      self.done = false;
      self.start = true;
      for i in 0..m { self.seq[i] = 0; self.c[i] = i }
      self.seq[m-1] = n;
      self.c[m-1] = m + n - 1;
      self.c[m] = 0;
   }
}


// ============================================================
// ------------------ nested combinations ---------------------
// ============================================================

use std::{mem, ptr};

pub fn multichoose<T>(seq: &'static [usize], elems: *const [*const T]) -> Multichoose<T> {
   assert!(seq.len() > 0);
   let n_seq = seq.len();
   let sum = seq.iter().sum();
   assert_eq!(unsafe { (*elems).len() }, sum);
   let mut result = block!(&'static [*const T], n_seq);

   let mut s = block!(MultichooseSeq<T>, n_seq - 1);
   let mut n = sum;
   for i in 0..n_seq - 1 {
      let x = MultichooseSeq {
         kc: k_combinations(n, seq[i]),
         result: block!(*const T, seq[i]),
         remaining: block!(*const T, n - seq[i]),
      };
      unsafe { ptr::write(&mut s[i], x); }
      //s.push(x);
      result[i] = s[i].result.get();
      n -= seq[i];
   }

   if n_seq > 1 {
      result[n_seq - 1] = s[n_seq - 2].remaining.get();
   } else {
      result[0] = unsafe { &(*elems) };
   }

   Multichoose { s, result, elems, s_idx: 0, done: false }
}

/// Iterator for all the ways to select first n_0 elements, then n_1 elements, and so on, from some set.
/// returns an array of arrays of elements.
pub struct Multichoose<T: 'static> {
   s: Block<MultichooseSeq<T>>,
   result: Block<&'static[*const T]>,
   elems: *const [*const T],
   s_idx: usize,
   done: bool,
}

impl<T> Drop for Multichoose<T> {
   fn drop(&mut self) {
      for i in 0..self.s.len() {
         unsafe { mem::drop(ptr::read(&self.s[i])); }
      }
   }
}

/// Data for a single sequence in a multichoose iter
struct MultichooseSeq<T: 'static> {
   kc: KCombinations,
   result: Block<*const T>,
   remaining: Block<*const T>,
}

impl<T: ::std::fmt::Display> Iterator for Multichoose<T> {
   type Item = &'static[&'static[*const T]];
   fn next(&mut self) -> Option<&'static[&'static[*const T]]> {
      if self.done { return None }
      let Multichoose{ ref mut s, elems, mut s_idx, .. } = *self;

      if s.is_empty() {
         // If there's only a single sequence (and so choices to make), we return the result then finish.
         self.done = true;
         return Some(self.result.get())
      }

      while s_idx < s.len() {
         if let Some(c) = s[s_idx].kc.next() {
            // calculate result & remaining
            let prev = if s_idx == 0 { unsafe {&*elems} } else { s[s_idx - 1].remaining.get() };

            let mut rem_idx = 0;
            let mut prev_idx = 0;
            for i in 0..c.len() {
               let idx = c[i];
               s[s_idx].result[i] = prev[idx];

               while prev_idx < idx {
                  s[s_idx].remaining[rem_idx] = prev[prev_idx];
                  rem_idx += 1;
                  prev_idx += 1;
               }
               prev_idx += 1;
            }

            while prev_idx < prev.len() {
               s[s_idx].remaining[rem_idx] = prev[prev_idx];
               rem_idx += 1;
               prev_idx += 1;
            }

            s_idx += 1;
         } else {
            if s_idx == 0 {
               self.done = true;
               return None;
            }
            s[s_idx].kc.reset();
            s_idx -= 1;
         }
      }
      self.s_idx = s_idx - 1;

      Some(self.result.get())
   }
}

// ============================================================
// ------------------------- tests ----------------------------
// ============================================================

#[test] fn test_multichoose() {
   multichoose::<usize>(&[0], &[]).count();
   multichoose(&[1], &[&0]).count();
   multichoose(&[1, 1], &[&0, &0]).count();
   multichoose(&[2], &[&0, &0]).count();
}

#[test] fn test_k_combinations() {
   assert_eq!(1, k_combinations(1, 1).count());
   assert_eq!(2, k_combinations(2, 1).count());
   assert_eq!(1, k_combinations(2, 2).count());

   assert_eq!(k_combinations(4, 3).map(|s| s.to_vec()).collect::<Vec<_>>(), vec![
      vec![0, 1, 2],
      vec![0, 1, 3],
      vec![0, 2, 3],
      vec![1, 2, 3],
   ]);
}

#[test] fn test_seq_combinations() {
   assert_eq!(1, sequence_combinations(1, 1).count());
   assert_eq!(1, sequence_combinations(2, 1).count());
   assert_eq!(2, sequence_combinations(1, 2).count());
   assert_eq!(3, sequence_combinations(2, 2).count());

   assert_eq!(sequence_combinations(3, 3).map(|s| s.to_vec()).collect::<Vec<_>>(), vec![
      vec![0, 0, 3],
      vec![0, 1, 2],
      vec![1, 0, 2],
      vec![0, 2, 1],
      vec![1, 1, 1],
      vec![2, 0, 1],
      vec![0, 3, 0],
      vec![1, 2, 0],
      vec![2, 1, 0],
      vec![3, 0, 0],
   ]);

   assert_eq!(sequence_combinations(2, 3).map(|s| s.to_vec()).collect::<Vec<_>>(), vec![
      vec![0, 0, 2],
      vec![0, 1, 1],
      vec![1, 0, 1],
      vec![0, 2, 0],
      vec![1, 1, 0],
      vec![2, 0, 0],
   ]);
}
