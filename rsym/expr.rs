use std::fmt;

pub use rug::*;
pub use rug::ops::*;
pub use rug::Integer as Int;

/// An expression.
#[derive(Clone, PartialEq, Eq, PartialOrd, Ord, EnumAsGetters, EnumIntoGetters, EnumToGetters, EnumIsA)]
pub enum Term {
   App(Application),
   Sym(Symbol),
   Integer(Int),
   Rational(Rational),
   Bool(bool),
   Str(String),
}

/// A function application
#[derive(Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct Application {
   pub op: Symbol,
   pub args: Vec<Term>,
}

/// A Symbol
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Symbol {
   pub sym: ::sym::Sym,
   // Use for scoping -- variables in global scope have n=0
   pub n: usize,
}

/// The attributes associated with a function symbol.
#[derive(Clone, Copy, Default)]
pub struct Attributes {
   pub hold_all: bool,
   pub hold_first: bool,
   pub hold_rest: bool,
   pub sequence_hold: bool,
   pub flat: bool,
   pub orderless: bool,
}

impl Term {
   /// Create a function application, given the operator and arguments.
   pub fn new<H: Into<Symbol>, B: Into<Vec<Term>>>(op: H, body: B) -> Term {
      Term::App(Application { op: op.into(), args: body.into() })
   }

   /// Create a symbol expression.
   pub fn sym<T: Into<Symbol>>(s: T) -> Term { Term::Sym(s.into()) }

   /// Create a rational expression.
   pub fn rational<T: Into<Rational>>(n: T) -> Term {
      let n = n.into();
      if *n.denom() == 1i32 { Term::Integer(n.numer().clone()) }
      else { Term::Rational(n) }
   }

   /// Create an integer expression.
   pub fn int<T: Into<Int>>(n: T) -> Term { Term::Integer(n.into()) }

   /// Create a boolean expression.
   pub fn bool<T: Into<bool>>(n: T) -> Term { Term::Bool(n.into()) }

   /// Create a string expression.
   pub fn str<T: Into<String>>(n: T) -> Term { Term::Str(n.into()) }

   /// Get the expression's head
   pub fn head(&self) -> Symbol {
      match *self {
         Term::Sym(_) => sym!("Symbol").into(),
         Term::Rational(_) => sym!("Rational").into(),
         Term::Integer(_) => sym!("Integer").into(),
         Term::Bool(_) => sym!("Boolean").into(),
         Term::Str(_) => sym!("String").into(),
         Term::App(ref v) => v.op.clone(),
      }
   }

   /// Get the expression's associated symbol.
   pub fn assoc(&self) -> Symbol {
      match *self {
         Term::Sym(ref x) => x.clone(),
         ref x => x.head(),
      }
   }

   /// Traverse the expression tree
   pub fn traverse<F: FnMut(&mut Term)>(&mut self, visit: &mut F) {
      visit(self);
      if let Term::App(ref mut e) = *self { for e in &mut e.args { e.traverse(visit); } }
   }

   /// Get a rational or integer expression as a number
   pub fn into_num(self) -> Rational {
      match self {
         Term::Rational(x) => x,
         Term::Integer(x) => x.into(),
         x => panic!("called into_num on non-numeric expression: {}", x),
      }
   }

   /// For a condition, pattern test, or named pattern, returns the inner pattern.
   pub fn nested_pattern(&self) -> Option<&Term> {
      if !self.is_app() { return None; }
      let a = self.as_app();
      match a.op.sym {
         sym!("Pattern") => Some(&a.args[1]),
         sym!("Condition") | sym!("PatternTest") => Some(&a.args[0]),
         _ => None
      }
   }

   /// Check if the expression is a blank or a nested pattern containing a blank
   pub fn is_blank(&self) -> bool {
      if let Some(p) = self.nested_pattern() { return p.is_blank(); }
      self.is_app() && self.as_app().op == sym!("Blank")
   }

   /// Check if the expression is a blank seq or a nested pattern containing a blank seq
   pub fn is_blank_seq(&self) -> bool {
      if let Some(p) = self.nested_pattern() { return p.is_blank_seq(); }
      self.is_app() && self.as_app().op == sym!("BlankSequence")
   }

   /// Check if the expression is a blank null seq or a nested pattern containing a blank mull seq
   pub fn is_blank_null_seq(&self) -> bool {
      if let Some(p) = self.nested_pattern() { return p.is_blank_null_seq(); }
      self.is_app() && self.as_app().op == sym!("BlankNullSequence")
   }
}

impl Application {
   pub fn flatten(&mut self, hold_seq: bool, flat: bool) {
      if hold_seq && !flat { return; }
      for i in (0..self.args.len()).rev() {
         let flatten = self.args[i].is_app() && {
            let h = &self.args[i].as_app().op;
            (!hold_seq && *h == sym!("Sequence")) || (flat && *h == self.op)
         };

         if flatten {
            let mut seq = self.args.remove(i).into_app().args;
            self.args.splice(i..i, seq.drain(..));
         }
      }
   }
}

impl<T: Into<::sym::Sym>> From<T> for Symbol {
   fn from(t: T) -> Symbol { Symbol { sym: t.into(), n: 0} }
}

/// Conversion from symbols to expressions.
impl<T: Into<Symbol>> From<T> for Term {
   fn from(t: T) -> Term { Term::Sym(t.into()) }
}

impl PartialEq<::sym::Sym> for Symbol {
   fn eq(&self, rhs: &::sym::Sym) -> bool { self.sym == *rhs }
}

impl PartialEq<Symbol> for Term {
   fn eq(&self, rhs: &Symbol) -> bool {
      match *self {
         Term::Sym(ref s) => s == rhs,
         _ => false
      }
   }
}

impl PartialEq<::sym::Sym> for Term {
   fn eq(&self, rhs: &::sym::Sym) -> bool {
      match *self {
         Term::Sym(ref s) => s.sym == *rhs,
         _ => false
      }
   }
}

impl fmt::Display for Term {
   fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
      match *self {
         Term::Sym(ref s) => write!(f, "{}", s.sym),
         Term::Rational(ref n) => write!(f, "{}", n),
         Term::Integer(ref n) => write!(f, "{}", n),
         Term::Bool(ref b) => write!(f, "{}", b),
         Term::Str(ref s) => write!(f, "\"{}\"", s.escape_default()),
         Term::App(ref v) => write!(f, "{}", v),
      }
   }
}

impl fmt::Debug for Term {
   fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> { write!(f, "{}", self) }
}

impl fmt::Debug for Application {
   fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> { write!(f, "{}", self) }
}

impl fmt::Display for Application {
   fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
      let infix = match self.op.sym {
         sym!("Times") => Some(" *"),
         sym!("Plus") => Some(" +"),
         sym!("Power") => Some(" ^"),
         _ => None,
      };

      if infix.is_some() {
         write!(f, "(")?;
      } else {
         write!(f, "{}[", self.op.sym)?;
      }

      if self.args.len() > 0 {
         let infix = infix.unwrap_or(",");
         for i in 0..(self.args.len() - 1) { write!(f, "{}{} ", self.args[i], infix)?; }
         write!(f, "{}", self.args[self.args.len() - 1])?;
      }

      if infix.is_some() {
         write!(f, ")")
      } else {
         write!(f, "]")
      }
   }
}
