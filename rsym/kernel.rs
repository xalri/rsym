use expr::*;
use mem::*;
use combinatorics::*;

use std::mem;
use std::vec::*;
use std::rc::Rc;
use fnv::FnvHashMap as HashMap;
use take_mut::take;

/// Evaluates expressions
pub struct Kernel {
   /// A stack of rewrite rules
   pub rule_stack: Vec<Rc<RewriteRule>>,
   /// Built in rules
   pub builtin: HashMap<::sym::Sym, fn(&mut Term) -> bool>,
   /// Rule associations
   pub assoc: HashMap<::sym::Sym, Box<Vec<usize>>>,
   /// Holds attributes associated with each symbol
   pub attributes: HashMap<::sym::Sym, Attributes>,
   /// Stack frames
   pub frames: Vec<usize>,

   // Tracks whether a single term is matched as a sequence variable or normal variable
   matched_blank_seq: bool,
}

/// A rewrite rule
pub struct RewriteRule{ pub pattern: Term, pub rewrite_fn: RewriteFn }

/// The replacement function for a rewrite rule.
pub type RewriteFn = Box<Fn(&mut Kernel) -> Term>;

impl Kernel {
   /// Create a new `Kernel`
   pub fn new() -> Kernel {
      Kernel {
         rule_stack: vec![],
         builtin: HashMap::default(),
         assoc: HashMap::default(),
         attributes: HashMap::default(),
         frames: vec![],
         matched_blank_seq: false,
      }
   }

   /// Evaluate an expression.
   pub fn eval(&mut self, mut expr: Term) -> Term {
      self.flatten(&mut expr);
      self._eval(expr)
   }

   pub fn flatten(&self, e: &mut Term) {
      if let Term::App(ref mut e) = e {
         let attrs = self.attrs(&e.op);
         e.flatten(attrs.sequence_hold, attrs.flat);
      }
   }

   /// Evaluate an expression by applying some slice of the rule stack
   pub fn _eval(&mut self, mut expr: Term) -> Term {
      if !(expr.is_app() || expr.is_sym()) { return expr }

      loop {
         let mut app_op = None;
         if let Term::App(ref mut e) = expr {
            app_op = Some(e.op.sym.clone());
            let attrs = self.attrs(&e.op);

            // Evaluate each term
            if !e.args.is_empty() && !attrs.hold_all && !attrs.hold_first {
               take(&mut e.args[0], |e| self._eval(e));
            }
            if !e.args.is_empty() && !attrs.hold_all && !attrs.hold_rest {
               for e in &mut e.args[1..] { take(e, |e| self.eval(e)); }
            }

            // Flatten
            e.flatten(attrs.sequence_hold, attrs.flat);

            // Sort
            if attrs.orderless { e.args.sort(); }
         }

         // Apply builtin functions
         if let Some(op) = app_op { if let Some(f) = self.builtin.get(&op) { if f(&mut expr) { break; } } }

         if !self.replace(&mut expr, 0) { break; }
      }
      return expr;
   }

   /// Apply rules from the given scope to all levels of an expression, repeat untill
   /// no more rules match. Ignores `HoldAll` etc.
   pub fn recursive_replace(&mut self, expr: &mut Term, scope: usize) {
      if let Term::App(Application {ref mut args, ..}) = expr {
         for e in args { self.recursive_replace(e, scope) }
      }
      while self.replace(expr, scope) {}
   }

   /// Apply the first matching rewrite rule in the given scope to update an expression,
   /// returns true if a rule was applied.
   pub fn replace(&mut self, expr: &mut Term, scope: usize) -> bool {
      let assoc = if let Some(a) = self.assoc.get_mut(&expr.assoc().sym) {
         if a.is_empty() { return false }
         else { a.as_mut() as *mut Vec<usize> }
      } else { return false };

      let mut n = unsafe { (*assoc).len() - 1 };
      let mut i = unsafe { (*assoc)[n] };

      while i >= scope {
         let rule = self.rule_stack[i].clone();
         assert_eq!(rule.pattern.assoc().sym, expr.assoc().sym);

         // Create a new stack frame for rules added by the replacement function
         self.push_stack();

         if self.matches(&rule.pattern, &[&*expr]) {
            // Apply the replacement function
            debug!("Found matching pattern {} -> {}", rule.pattern, expr);
            mem::replace(expr, (rule.rewrite_fn)(self));

            // Apply the substitution
            let f = self.frame_idx();
            self.recursive_replace(expr, f);
            debug!("Replaced with {}", expr);

            self.pop_stack();
            return true;
         }

         self.pop_stack();
         if n == 0 { break }
         n -= 1;
         i = unsafe { (*assoc)[n] };
      }

      return false;
   }

   /// Test if the given pattern matches a sequence of expressions.
   pub fn matches(&mut self, pattern: &Term, seq: &[&Term]) -> bool {
      // Atomic patterns match iff they're exactly equal to the expression
      // (Note that blanks are always compound expressions)
      if !pattern.is_app() { return seq.len() == 1 && pattern == seq[0]; }

      let pattern = pattern.as_app();

      // TODO: Inside assoc symbols blanks should match like blank seqs (if the inner & outer have the same head)

      // Match blank sequences
      if pattern.op == sym!("BlankSequence") {
         if seq.len() == 0 { return false }
         self.matched_blank_seq = true;
         return pattern.args.is_empty() || seq.iter().all(|e| *pattern.args[0].as_sym() == e.head());
      }
      if pattern.op == sym!("BlankNullSequence") {
         self.matched_blank_seq = true;
         return pattern.args.is_empty() || seq.iter().all(|e| *pattern.args[0].as_sym() == e.head());
      }

      // For `Pattern`s, test the inner pattern, if it matches add a rewrite rule replacing the
      // name with the matched expression or sequence.
      if pattern.op == sym!("Pattern") && pattern.args.len() == 2 {
         let name = pattern.args[0].as_sym();
         self.matched_blank_seq = false;

         if self.matches(&pattern.args[1], seq) {
            // If a single expr matches a blank sequence we want to assign to a sequence.
            let e = if seq.len() == 1 && !self.matched_blank_seq {
               seq[0].clone()
            } else {
               Term::new("Sequence", seq.iter().map(|&e| e.clone()).collect::<Vec<_>>())
            };

            // If the name has already been bound, check if the bound expressions are equal.
            let frame = self.frame_idx();
            if let Some(value) = self._get(name, frame) { return value == e; }

            self.local_rule(Term::Sym(name.clone()), Box::new(move |_| e.clone()));
            return true;
         } else {
            return false;
         }
      }

      // For conditional patterns, test the inner pattern then return the result of the condition
      if pattern.op == sym!("Condition") && pattern.args.len() == 2 {
         return self.matches(&pattern.args[0], seq) &&
                self.eval(pattern.args[1].clone()) == Term::bool(true);
      }

      // For matching pattern tests, apply the test to each expression in the sequence
      if pattern.op == sym!("PatternTest") && pattern.args.len() == 2 {
         let test = pattern.args[1].as_sym();
         return self.matches(&pattern.args[0], seq) &&
            seq.iter().all(|&e| self.eval(Term::new(test.clone(), vec![e.clone()])) == Term::bool(true));
      }

      // Nothing else can match a _sequence_ of expressions
      if seq.len() != 1 { return false; }
      let expr = seq[0];

      // If the pattern is a blank with the correct op, return true.
      if pattern.op == sym!("Blank") {
         return pattern.args.is_empty() || *pattern.args[0].as_sym() == expr.head();
      }

      // Atomic expressions can't match non-blank sequence patterns
      // (The expression `a` will never match a pattern `f[...]` unless f is `Blank`)
      if !expr.is_app() { return false; }
      let expr = expr.as_app();

      // Test the ops
      if pattern.op != expr.op { return false; }

      // Empty patterns only match empty targets
      if pattern.args.is_empty() { return expr.args.is_empty(); }

      // Finally attempt to find substitutions for f[a,b,c,..] -> f[x,y,z,..]
      // where a,b,c,.. may be sequence variables, taking into account orderlessness.
      let attrs = self.attrs(&expr.op);
      self.match_seq(&pattern.args, &expr.args, &pattern.op, attrs.orderless, attrs.flat)
   }

   /// Match a sequence of patterns to a sequence of expressions, where the terms of the pattern
   /// may be sequence variables. If orderless is true, tests every permutation of the pattern's terms.
   fn match_seq(&mut self, pattern: &[Term], target: &[Term], op: &Symbol, orderless: bool, flat: bool) -> bool {
      // Calculate the number of free arguments = the number of terms of the target that can match
      // to any of the sequence variables in the pattern.

      let star_vars = pattern.iter().filter(|t| t.is_blank_null_seq()).count();
      // If there aren't enough terms in the target to match this pattern, return false
      if target.len() < pattern.len() - star_vars {
         return false;
      }

      let plus_vars = pattern.iter().filter(|t|
         t.is_blank_seq() || (flat && t.is_blank())
      ).count();

      if star_vars + plus_vars == 0 && target.len() > pattern.len() { return false; }

      let free_args = target.len() + star_vars - pattern.len();

      // Make an array of references to terms of the target, since `match` takes &[&Term].
      // We take slices of this array for each term of the pattern.
      let mut terms = block!(*const Term, target.len());
      for i in 0..target.len() { terms[i] = &target[i] }

      if !orderless {
         // For ordered patterns we iterate through different ways to arrange the number of
         // free arguments among the pattern's sequence variables, and attempt to match each one.

         'seq_loop: for c in sequence_combinations(free_args, star_vars + plus_vars) {
            // Store the number of rules in the stack. Used to remove anything added while attempting
            // to match each term if any part of the pattern doesn't match.
            let scope = self.rule_stack.len();

            let mut n = 0;
            let mut c_idx = 0;

            for i in 0..pattern.len() {
               // Check if this pattern is a sequence pattern.
               let pattern = &pattern[i];
               let star = pattern.is_blank_null_seq();
               let plus = pattern.is_blank_seq() || (flat && pattern.is_blank());

               // Get the sequence of terms of the target to attempt to match this pattern
               let seq = if star || plus {
                  let start = n;
                  n += c[c_idx] + if plus { 1 } else { 0 };
                  c_idx += 1;
                  &terms[start..n]
               } else {
                  n += 1;
                  &terms[n - 1..n]
               };

               if !self.matches(pattern, unsafe{ mem::transmute(seq) }) {
                  // If it doesn't match, remove anything it added to the substitution then
                  // attempt the next sequence combination.
                  self.frames.push(scope);
                  self.pop_stack();
                  continue 'seq_loop;
               }
            }

            // If everything matches, return true, keeping the modified substitution in the
            // caller's stack frame.
            return true;
         }
      } else {
         let mut seq = block!(usize, pattern.len());

         if op == &Symbol::from("split") {
            let mut bind = vec![vec![]; pattern.len()];

            'a: for t in target {
               for i in 0..pattern.len() {
                  if self.matches(&pattern[i].as_app().args[1], &[t]) {
                     bind[i].push(t.clone());
                     continue 'a;
                  }
               }
               return false;
            }

            for i in (0..pattern.len()).rev() {
               let e = Term::new("Sequence", bind.pop().unwrap());
               let name = pattern[i].as_app().args[0].clone();
               self.local_rule(name, Box::new(move |_| e.clone()));
            }
            return true;
         }

         // TODO: start by testing constants in the pattern.

         for c in sequence_combinations(free_args, star_vars + plus_vars) {
            // Map the combination to one including non-sequence terms & store in `partition`
            let mut c_idx = 0;
            for i in 0..pattern.len() {
               let pattern = &pattern[i];
               let star = pattern.is_blank_null_seq();
               let plus = pattern.is_blank_seq() || (flat && pattern.is_blank());

               seq[i] = if star || plus {
                  c_idx += 1;
                  c[c_idx-1] + if plus { 1 } else { 0 }
               } else {
                  1 // non-sequence patterns match a single term of the target
               };
            }

            'part_loop: for partition in multichoose(seq.get(), terms.get()) {
               let scope = self.rule_stack.len();

               for i in 0..pattern.len() {
                  if !self.matches(&pattern[i], unsafe{ mem::transmute(partition[i]) }) {
                     self.frames.push(scope);
                     self.pop_stack();
                     continue 'part_loop;
                  }
               }
               return true;
            }
         }
      }

      false
   }

   /// Add a rewrite rule to the global stack frame
   pub fn global_rule(&mut self, pattern: Term, rewrite_fn: RewriteFn) {
      let idx = self.frames.get(0usize).cloned().unwrap_or(self.rule_stack.len());

      if !self.rule_stack.is_empty() {
         let mut i = self.rule_stack.len() - 1;
         let mut pos = HashMap::default();
         while i >= idx {
            let op = self.rule_stack[i].pattern.assoc();
            if !pos.contains_key(&op.sym) {
               pos.insert(op.sym.clone(), self.assoc(&op).len());
            }
            let j = pos.get_mut(&op.sym).unwrap();
            *j -= 1;
            self.assoc(&op)[*j] += 1;
            i -= 1;
         }
      }

      for idx in &mut self.frames { *idx += 1; }

      self.assoc(&pattern.assoc()).push(idx);
      self.rule_stack.insert(idx, Rc::new(RewriteRule{ pattern, rewrite_fn }));
   }

   pub fn validate_assoc(&self) {
      for (op, indices) in &self.assoc {
         for &i in indices.iter() {
            assert_eq!(self.rule_stack[i].pattern.assoc().sym, *op);
         }
      }
   }

   /// Add a rewrite rule to the current stack frame
   pub fn local_rule(&mut self, pattern: Term, rewrite_fn: RewriteFn) {
      let idx = self.rule_stack.len();
      self.assoc(&pattern.assoc()).push(idx);
      self.rule_stack.push(Rc::new(RewriteRule{pattern, rewrite_fn}));
   }

   pub fn attrs(&self, sym: &Symbol) -> Attributes {
      let sym = &sym.sym;
      if let Some(&set) = self.attributes.get(sym) { set }
      else { Attributes::default() }
   }

   pub fn mut_attrs(&mut self, sym: &Symbol) -> &mut Attributes {
      self.attributes.entry(sym.sym.clone()).or_default()
   }

   /// Get the value of a symbol, panics if not found.
   pub fn get<T: Into<Symbol>>(&mut self, sym: T) -> Term {
      self._get(&sym.into(), 0).expect("get called with unknown symbol")
   }

   /// Search the given scope for a symbol
   fn _get(&mut self, sym: &Symbol, scope: usize) -> Option<Term> {
      for i in (scope..self.rule_stack.len()).rev() {
         let rc = self.rule_stack[i].clone();
         if rc.pattern == *sym { return Some((rc.rewrite_fn)(self)); }
      }
      None
   }

   /// Create a new frame on the rule stack
   pub fn push_stack(&mut self) -> usize {
      self.frames.push(self.rule_stack.len());
      self.rule_stack.len()
   }

   /// Get the indices of the rules associated with a symbol
   pub fn assoc(&mut self, op: &Symbol) -> &mut Vec<usize> {
      self.assoc.entry(op.sym.clone()).or_default()
   }

   /// Pop the current rule stack frame
   pub fn pop_stack(&mut self) {
      let len = self.frames.pop().unwrap();
      while self.rule_stack.len() > len {
         let a = self.rule_stack.pop().unwrap();
         self.assoc(&a.pattern.assoc()).pop();
      }
      assert_eq!(self.rule_stack.len(), len);
   }

   /// The index into the stack for the top of the current frame
   pub fn frame_idx(&self) -> usize {
      *self.frames.last().unwrap()
   }
}
