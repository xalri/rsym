
/*

use expr::*;
use fnv::FnvHashMap as HashMap;

/// A substitution from variables to terms
pub struct Substitution {
   // Substitutions are usually small, so a linear search on a vector is faster than a hash map.
   stack: Vec<(Symbol, Expr)>,
   marks: Vec<usize>,
}

impl Substitution {
   /// Create a new substitution
   pub fn new() -> Substitution {
      Substitution { stack: Vec::with_capacity(64), marks: Vec::with_capacity(64) }
   }

   /// Get the substitution for a symbol
   pub fn get(&self, s: &Symbol) -> Option<&Expr> {
      self.stack.iter().find(|&(ref sym, _)| sym == s).map(|&(_, ref term)| term)
   }

   /// Mark a point to later revert or commit
   fn mark(&mut self) { self.marks.push(self.stack.len()) }

   /// Revert the substitution to it's state from the last mark
   fn revert(&mut self) { self.stack.truncate(self.marks.pop().unwrap()) }

   /// Commit the last mark
   fn commit(&mut self) { self.marks.pop().unwrap() }
}

/// Takes a pattern and returns a function which finds a substitution that transforms the pattern
/// into a given expression if such a substitution exists.
pub fn compile_pattern<F: Fn(&Symbol) -> Attributes>(pattern: Expr, attrs: F) -> impl Fn(&Expr) -> Option<Substitution> {

}
*/
