/// Memory management for combinatoric iterators

use std::ops::*;
use std::mem::*;
use std::cmp::*;
use std::cell::*;

const DEFAULT_SIZE: usize = 1024;

/// block(T, n) returns a mutable reference to a block of memory for holding
/// n elements of type T. The allocated memory is reused over multiple
/// instances of this call to `block!` on the same thread.
///
/// ** `Block`s must be dropped in reverse order they were created, never
///    store them on the heap **
///
/// Items added to the block are only dropped when overwritten by another
/// value or the thread exits (so it should only be used for copy types).
pub macro block($t:ty, $size:expr) {{
   thread_local!(static CHUNKS: UnsafeCell<Vec<usize>> = UnsafeCell::new(vec![]));

   #[allow(unused_unsafe)]
   CHUNKS.with(|r| {
      let chunks: &mut Vec<Chunk<$t>> = unsafe{ transmute(&mut *r.get()) };
      for c in chunks.iter_mut() { if let Some(block) = c.block($size) { return block; } }
      let chunk_size = max(DEFAULT_SIZE, $size);
      let mut mem = Vec::with_capacity(chunk_size);
      unsafe{ mem.set_len(chunk_size); }
      chunks.push(Chunk{ mem, used: 0 });
      chunks.last_mut().unwrap().block($size).unwrap()
   })
}}

/// A fixed-size chunk of thread-local memory
pub struct Chunk<T> { mem: Vec<T>, used: usize }

/// A reference to a block of shared memory
pub struct Block<T: 'static>(pub &'static mut [T], &'static mut usize);

impl<T> Chunk<T> {
   /// Create a new block in this chunk. Returns None if there's not enough space.
   pub fn block(&mut self, size: usize) -> Option<Block<T>> {
      let new_used = self.used + size;
      if new_used <= self.mem.capacity() {
         let mem: &'static mut [T] = unsafe{ transmute(&mut self.mem[self.used..new_used]) };
         self.used = new_used;
         let used_ptr: *mut usize = &mut self.used;
         Some(Block(mem, unsafe{ &mut *used_ptr }))
      } else {
         None
      }
   }
}

impl<T: 'static> Block<T> {
   /// Get a 'static reference to the memory.
   pub fn get(&self) -> &'static [T] {
      unsafe{ ::std::ptr::read(transmute(&self.0)) }
   }
}

// Deref & Deref mut to the inner slice.
impl<T> Deref for Block<T> {
   type Target = [T];
   fn deref(&self) -> &[T] { &self.0 }
}

impl<T> DerefMut for Block<T> {
   fn deref_mut(&mut self) -> &mut [T] { &mut self.0 }
}

// Decrement the used counter on drop.
impl<T> Drop for Block<T> {
   fn drop(&mut self) {
      *self.1 -= self.0.len()
   }
}

#[test] fn block_allocation() {
   fn recurse(n: usize, v: usize) {
      if n == 0 { return };
      let mut hmm = block!(usize, n);
      hmm[0] = v;
      recurse(n - 1, v);
   }
   recurse(100, 1);
   recurse(20, 2);
}