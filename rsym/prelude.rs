use kernel::*;
use expr::*;
use take_mut::*;
use grammar;

/// Initialise the given kernel with all the built-in functions.
pub fn prelude(k: &mut Kernel) {
   minimal(k);

   // include_str adds the contents of the file to the program text at compile time.
   k.eval(grammar::expr(include_str!("rules/attributes.rsym")).unwrap());
   k.eval(grammar::expr(include_str!("rules/general.rsym")).unwrap());
   k.eval(grammar::expr(include_str!("rules/query.rsym")).unwrap());
   k.eval(grammar::expr(include_str!("rules/simplify.rsym")).unwrap());
   k.eval(grammar::expr(include_str!("rules/differentiation.rsym")).unwrap());
   k.eval(grammar::expr(include_str!("rules/trigonometry.rsym")).unwrap());

   // TODO: scoping for names bound by builtins
   relational_ops(k);
   boolean_ops(k);
   arithmetic_ops(k);
   general(k);
}

/// Adds the builtin functions necessary for creating rewrite rules.
fn minimal(e: &mut Kernel) {
   // Setting Attributes
   e.global_rule(
      grammar::expr("SetAttributes[ff:_Symbol, aa:_Symbol]").unwrap(),
      Box::new(|e| {
         let function = e.get("ff").into_sym();
         let attr = e.get("aa").into_sym();
         debug!("Setting attribute {} for {}", attr.sym, function.sym);
         let attrs = e.mut_attrs(&function);
         match attr.sym {
            sym!("HoldAll") => attrs.hold_all = true,
            sym!("HoldFirst") => attrs.hold_first = true,
            sym!("HoldRest") => attrs.hold_rest = true,
            sym!("SequenceHold") => attrs.sequence_hold = true,
            sym!("Flat") => attrs.flat = true,
            sym!("Orderless") => attrs.orderless = true,
            x => warn!("unknown attribute {}", x),
         }
         sym!("Null").into()
      })
   );
   e.global_rule(
      grammar::expr("ClearAttributes[ff:_Symbol]").unwrap(),
      Box::new(|e| {
         let function = e.get("ff").into_sym();
         debug!("Clearing attributes for {}", function.sym);
         *e.mut_attrs(&function) = Attributes::default();
         sym!("Null").into()
      })
   );

   // Function for evaluating a `Set` or `SetDelayed` expression, returns the pattern
   // and replacement as a pair with the unique variable names.
   fn eval_set(k: &mut Kernel) -> (Term, Term) {
      let mut pattern = k.get("pp");
      let mut replace = k.get("rr");

      // Evaluate the arguments of the left hand side.
      if let Term::App(ref mut pattern) = pattern {
         for e in &mut pattern.args { take(e, |e| k.eval(e)); }
      }

      // Flatten the lhs
      k.flatten(&mut pattern);

      // Create a stack frame to hold name replacement rules for scoping
      k.push_stack();

      // Find all the local variables in the pattern, and build a replacement rule to make them
      // unique to this scope.
      pattern.traverse(&mut |e: &mut Term| {
         if let Term::App(ref pattern) = *e {
            if pattern.op == sym!("Pattern") {
               let name = pattern.args[0].as_sym().clone();
               // n is unique among nested `Set` expressions but the same within a single pattern.
               let scoped = Term::sym(Symbol { sym: name.sym.clone(), n: k.frames.len() });

               k.local_rule(Term::sym(name), Box::new(move |_| scoped.clone()));
            }
         }
      });

      // Apply the replacement rules
      let scope = k.frame_idx();
      k.recursive_replace(&mut pattern, scope);
      k.recursive_replace(&mut replace, scope);

      k.pop_stack();

      debug!("Adding rule {} = {}", pattern, replace);
      (pattern, replace)
   }

   e.global_rule(
      grammar::expr("Set[pp:_, rr:_]").unwrap(),
      Box::new(|k: &mut Kernel| {
         let (pattern, replace) = eval_set(k);
         let result = replace.clone();
         k.global_rule(pattern, Box::new(move |_| replace.clone()));
         // Set returns the evaluated form of it's right hand side, useful for memoization
         result
      })
   );

   e.global_rule(
      grammar::expr("SetDelayed[pp:_, rr:_]").unwrap(),
      Box::new(|k: &mut Kernel| {
         let (pattern, replace) = eval_set(k);
         k.global_rule(pattern, Box::new(move |_| replace.clone()));
         sym!("Null").into()
      })
   );

   // Compound expressions
   e.global_rule(
      grammar::expr("CompoundExpression[ee:___, tt:_]").unwrap(),
      Box::new(|kernel| {
         let mut exprs = kernel.get("ee").into_app();
         let tail = kernel.get("tt");

         for e in exprs.args.drain(..) { kernel.eval(e); }
         kernel.eval(tail)
      })
   );
}


/// Defines relational operators
fn relational_ops(k: &mut Kernel) {
   k.global_rule(
      grammar::expr("Equal[aa:_, bb:_]").unwrap(),
      Box::new(|e| Term::bool(e.get("aa") == e.get("bb")))
   );

   k.global_rule(grammar::expr("Less[aa:_, bb:_]").unwrap(), Box::new(|e| Term::bool(e.get("aa") < e.get("bb"))));
   k.global_rule(grammar::expr("Greater[aa:_, bb:_]").unwrap(), Box::new(|e| Term::bool(e.get("aa") > e.get("bb"))));
   k.global_rule(grammar::expr("LessEqual[aa:_, bb:_]").unwrap(), Box::new(|e| Term::bool(e.get("aa") <= e.get("bb"))));
   k.global_rule(grammar::expr("GreaterEqual[aa:_, bb:_]").unwrap(), Box::new(|e| Term::bool(e.get("aa") >= e.get("bb"))));
}


/// Defines boolean operators
fn boolean_ops(k: &mut Kernel) {
   // TODO: n-ary forms

   k.global_rule(
      grammar::expr("And[aa_Boolean, bb_Boolean]").unwrap(),
      Box::new(|e| {
         Term::bool(e.get("aa").into_bool() && e.get("bb").into_bool())
      })
   );

   k.global_rule(
      grammar::expr("Or[aa:_Boolean, bb:_Boolean]").unwrap(),
      Box::new(|e| {
         Term::bool(e.get("aa").into_bool() || e.get("bb").into_bool())
      })
   );

   k.global_rule(
      grammar::expr("Not[aa:_Boolean]").unwrap(),
      Box::new(|e| {
         Term::bool(!e.get("aa").into_bool())
      })
   );
}


/// Defines arithmetic operators (+, *, ^, Mod)
fn arithmetic_ops(k: &mut Kernel) {

   k.global_rule(
      grammar::expr("Plus[aa_?NumberQ, bb_?NumberQ, tt___]").unwrap(),
      Box::new(|e| {
         let mut tail = e.get("tt").into_app();
         let e = Term::rational(e.get("aa").into_num() + e.get("bb").into_num());
         if tail.args.is_empty() { e } else { tail.args.push(e); Term::new("Plus", tail.args) }
      })
   );

   k.global_rule(
      grammar::expr("Times[aa_?NumberQ, bb_?NumberQ, tt___]").unwrap(),
      Box::new(|e| {
         let mut tail = e.get("tt").into_app();
         let e = Term::rational(e.get("aa").into_num() * e.get("bb").into_num());
         if tail.args.is_empty() { e } else { tail.args.push(e); Term::new("Times", tail.args) }
      })
   );

   k.global_rule(
      grammar::expr("Power[aa:_?NumberQ, -1]").unwrap(),
      Box::new(|e| Term::rational(e.get("aa").into_num().recip()) )
   );

   k.global_rule(
      grammar::expr("Power[aa:_?NumberQ, bb:_Integer /; bb >= 0]").unwrap(),
      Box::new(|e| Term::rational(e.get("aa").into_num().pow(e.get("bb").into_integer().to_u32().unwrap())))
   );

   k.global_rule(
      grammar::expr("Power[tt:___, aa:_?NumberQ, bb:_Integer /; bb >= 0]").unwrap(),
      Box::new(|e| {
         let mut tail = e.get("tt").into_app();
         let e = Term::rational(e.get("aa").into_num().pow(e.get("bb").into_integer().to_u32().unwrap()));
         if tail.args.is_empty() { e } else {
            tail.args.push(e);
            Term::new("Power", tail.args)
         }
      })
   );

   k.global_rule(
      grammar::expr("Mod[aa:_Integer, bb:_Integer]").unwrap(),
      Box::new(|e| Term::rational(e.get("aa").into_integer() % e.get("bb").into_integer()) )
   );

   k.global_rule(
      grammar::expr("OddQ[aa:_Integer]").unwrap(),
      Box::new(|e| Term::bool(e.get("aa").into_integer().is_odd()) )
   );

   k.global_rule(
      grammar::expr("EvenQ[aa:_Integer]").unwrap(),
      Box::new(|e| Term::bool(e.get("aa").into_integer().is_even()) )
   );
}

fn general(k: &mut Kernel) {
   k.global_rule(
      grammar::expr("Assert[aaa_]").unwrap(),
      Box::new(|e| {
         let a = e.get("aaa");
         let b = e.eval(a.clone());
         if b != Term::Bool(true) { panic!("Assertion failed, {}  -> {}", a, b) }
         Term::sym(sym!("Null"))
      })
   );

   k.global_rule(
      grammar::expr("Print[aaa_String]").unwrap(),
      Box::new(|e| {
         println!("{}", e.get("aaa").as_str());
         Term::sym(sym!("Null"))
      })
   );

   k.global_rule(
      grammar::expr("Sort[List[aaa___]]").unwrap(),
      Box::new(|e| {
         let mut a = e.get("aaa").to_app();
         a.args.sort();
         a.op = sym!("List").into();
         let a = Term::App(a);
         a
      })
   );
}
