#![feature(decl_macro)]

extern crate rsym;

use rsym::expr::*;
use rsym::grammar;

macro expr($a:expr) { grammar::expr($a).unwrap() }
macro assert_expr_eq($a:expr, $b:expr) { assert_eq!(expr!($a), expr!($b)) }

#[test] fn grammar() {
   use rsym::kernel::*;
   use rsym::prelude::*;

   let mut k = Kernel::new();
   prelude(&mut k);
   k.eval(grammar::expr(include_str!("test_grammar.rsym")).unwrap());
}

#[test] fn parse_symbol() {
   assert_eq!(grammar::symbol("test"), Ok(Symbol::from("test")));
   assert_eq!(grammar::symbol("Plus"), Ok(Symbol::from("Plus")));
   assert!(grammar::symbol("1test").is_err());
   assert!(grammar::symbol("test1").is_err());
   assert!(grammar::symbol("_test").is_err());
   assert!(grammar::symbol("test_").is_err());
   assert!(grammar::symbol("t-est").is_err());
}

#[test] fn parse_number() {
   assert_eq!(grammar::integer("42").unwrap(), Int::from(42));
   assert_eq!(grammar::rational("42.5").unwrap(), Rational::from_f64(42.5).unwrap());
   assert!(grammar::rational("42.").is_err());
   assert!(grammar::rational(".42").is_err());
}

#[test] fn parse_composite() {
   let add_empty = Term::new("Plus", vec![]);

   assert_eq!(grammar::expr("Plus[]").unwrap(), add_empty);
   assert_eq!(grammar::expr("Plus[ ]").unwrap(), add_empty);
   assert_eq!(grammar::expr("Plus [ ]").unwrap(), add_empty);
   assert_eq!(grammar::expr(" Plus [ ] ").unwrap(), add_empty);

   assert!(grammar::expr("Plus[").is_err());
   assert!(grammar::expr("Plus]").is_err());
   assert!(grammar::expr("Plus[,]").is_err());
   assert!(grammar::expr("Plus[a,]").is_err());

   let add_ab = Term::new("Plus", vec![Term::sym("a"), Term::sym("b")]);

   assert_eq!(grammar::expr("Plus[a,b]").unwrap(), add_ab);
   assert_eq!(grammar::expr("Plus[ a , b ]").unwrap(), add_ab);
}

#[test] fn parse_binary_infix() {
   assert_eq!(grammar::expr("1 + 2").unwrap(),
              grammar::expr("Plus[1, 2]").unwrap());

   assert_eq!(grammar::expr("a + b").unwrap(),
              grammar::expr("Plus[a, b]").unwrap());

   assert_eq!(grammar::expr("a + b * c").unwrap(),
              grammar::expr("Plus[a, Times[b, c]]").unwrap());

   assert_eq!(grammar::expr("a * b + c").unwrap(),
              grammar::expr("Plus[Times[a, b], c]").unwrap());
}

#[test] fn parse_unary_prefix() {
   expr!("-2");
   expr!("- 2");
   expr!(" - 2");
   expr!("-a");
   expr!("-Plus[a, b]");
   expr!("-Plus[a, b]");
   expr!("-a * b");
   expr!("-(a * -b)");

   assert_expr_eq!("_Integer", "Blank[Integer]");
   assert_expr_eq!("__", "BlankSequence[]");
   assert_expr_eq!("___Integer", "BlankNullSequence[Integer]");
}

#[test] fn parse_pattern() {
   assert_eq!(grammar::expr("a:_Integer").unwrap(),
              grammar::expr("Pattern[a, Blank[Integer]]").unwrap());

   assert_eq!(grammar::expr("a:(_+_)").unwrap(),
              grammar::expr("Pattern[a, Plus[Blank[], Blank[]]]").unwrap());

   assert_eq!(grammar::expr("a_ + b_").unwrap(),
              grammar::expr("Plus[Pattern[a, Blank[]], Pattern[b, Blank[]]]").unwrap());

   assert_eq!(grammar::expr("a:_ + b:_").unwrap(),
              grammar::expr("Plus[Pattern[a, Blank[]], Pattern[b, Blank[]]]").unwrap());

   assert_eq!(grammar::expr("{a__, b:__}").unwrap(),
              grammar::expr("List[Pattern[a, BlankSequence[]], Pattern[b, BlankSequence[]]]").unwrap());

   assert_eq!(grammar::expr("{a___, b:___}").unwrap(),
              grammar::expr("List[Pattern[a, BlankNullSequence[]], Pattern[b, BlankNullSequence[]]]").unwrap());
}

