#![feature(decl_macro)]

extern crate rsym;
extern crate badlog;
#[macro_use] extern crate log;

use rsym::grammar;
use rsym::prelude::*;
use rsym::kernel::*;

#[test] fn assertions_match() {
   // Make sure assertions do something, or the other tests are pointless ._.
   let mut k = Kernel::new();
   prelude(&mut k);

   let e = grammar::expr("Assert[true]").unwrap();
   let pattern = grammar::expr("Assert[_]").unwrap();

   // First, pattern matching must work.
   assert!(k.matches(&pattern, &[&e]));

   // Then the evaluation procedure must find the matching pattern in the prelude, and apply
   // the replacement to give Null.
   assert_eq!(k.eval(e), grammar::expr("Null").unwrap());
}

#[test] fn language() {
   let mut k = Kernel::new();
   prelude(&mut k);
   k.eval(grammar::expr(include_str!("test_language.rsym")).unwrap());
}

#[test] fn simplification() {
   let mut k = Kernel::new();
   prelude(&mut k);
   k.eval(grammar::expr(include_str!("test_simplification.rsym")).unwrap());
}

#[test] fn differentiation() {
   let mut k = Kernel::new();
   prelude(&mut k);
   k.eval(grammar::expr(include_str!("test_differentiation.rsym")).unwrap());
}
